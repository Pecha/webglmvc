﻿VueGL.VglClickableMesh = {
    mixins: VueGL.VglMesh.mixins,
    props: ['onclick', "geometry", "material"],
    computed: {
        inst: () => new THREE.Mesh()
    },
    created: function () {
        this.inst.onclick = this.$props.onclick;
    },
    name: "VglClickableMesh"
};

VueGL.VglTransparentMaterial = {
    mixins: VueGL.VglMeshStandardMaterial.mixins,
    props: ['opacity', "color", "name"],
    computed: {
        inst: () => new THREE.MeshStandardMaterial()
    },
    watch: {
        inst: {
            handler: function (inst) { inst.color.setStyle(this.color); },
            immediate: true,
        },
        color: function (newColor) {
            this.inst.color.setStyle(newColor);
            this.update();
        }
    },
    created: function () {
        this.inst.depthWrite = false;
        this.inst.transparent  = true;
        this.inst.opacity = this.$props.opacity;
    },
    name: "VglTransparentMaterial"
};

Object.keys(VueGL).forEach((key) => {
    Vue.component(key, VueGL[key]);
});

function OrbitCamera(canvasId) {
    var canvas = document.getElementById(canvasId);
    var controls = new THREE.OrbitControls(canvas.__vue__.vglNamespace.cameras.active, canvas);
    function update() {
        requestAnimationFrame(update);
        controls.update();
        canvas.__vue__.vglNamespace.update();
    }
    update();
}

function RayCasterClicker(canvasId, scene) {
    var canvas = document.getElementById(canvasId);
    var raycaster = new THREE.Raycaster();
    var mouse = new THREE.Vector2();
    canvas.addEventListener("click", function (e) {
        var rect = canvas.getBoundingClientRect();
        mouse.x = ((event.clientX - rect.left) / rect.width) * 2 - 1;
        mouse.y = - ((event.clientY - rect.top) / rect.height) * 2 + 1;
        raycaster.setFromCamera(mouse, canvas.__vue__.vglNamespace.cameras.active);
        var intersects = raycaster.intersectObjects(canvas.__vue__.vglNamespace.scenes[scene].children, true);
        if (intersects.length > 0) {
            var i = 0, clicked = false;
            while (i < intersects.length || !clicked) {
                if (intersects[i].object.onclick) {
                    clicked = true;
                    intersects[i].object.onclick();
                }
                i++;
            }
        }
    });
}

ready(function () {
    new OrbitCamera("canvas1");
    new OrbitCamera("canvas2");
    new OrbitCamera("canvas3");
    new RayCasterClicker("canvas3", "leds");
});


var app1 = new Vue({
    el: '#app1',
    data: {
        text: 'Hello world!',
        diffuseColor: '#dd3300',
        ambientColor: '#ee9966'
    }
});

var app2 = new Vue({
    el: '#app2',
    data: {
        items: [],
    },
    methods: {
        add: function () {
            this.items.push({
                size: { x: 1, y: 1, z: 1 },
                position: { x: 0, y: 0, z: 0 },
                color: getRandomColor()
            });
        },
        remove: function (index) {
            this.items.splice(index, 1);
        }
    },
    created: function () {
        this.add();
        this.add();
    }
});

var app3 = new Vue({
    el: '#app3',
    data: {
        leds: [
            { state: false },
            { state: true },
            { state: false },
            { state: false },
            { state: true },
            { state: true },
            { state: false },
            { state: false },
        ],
        color: '#ff0000',
    },
    computed: {
        int: {
            get: function () {
                var r = 0;
                for (var i = 0; i < this.leds.length; i++) {
                    r += this.leds[i].state ? Math.pow(2, i) : 0;
                }
                return r;
            },
            set: function (v) {
                var bin = toBin(v);
                for (var i = 0; i < this.leds.length; i++) {
                    this.leds[this.leds.length - i - 1].state = bin[i] == "1";
                }
            }
        },
        hex: {
            get: function () { return toHex(this.int); },
            set: function (v) { this.int = parseInt(v, 16); }
        },
        bin: {
            get: function () { return toBin(this.int); },
            set: function (v) { this.int = parseInt(v, 2); }
        }
    },
    methods: {
        lightSwitch: function (index) {
            this.leds[index].state = !this.leds[index].state;
        },
        inst: function (mesh) {
            console.log(mesh);
        }
    }
});