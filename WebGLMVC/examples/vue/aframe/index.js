﻿var app1 = new Vue({
    el: '#app1',
    data: {
        text: 'Hello world!',
        diffuseColor: '#dd3300'
    }
});



var app2 = new Vue({
    el: '#app2',
    data: {
        items: [],
    },
    methods: {
        add: function () {
            this.items.push({
                size: { x: 1, y: 1, z: 1 },
                position: { x: 0, y: 0, z: 0 },
                color: getRandomColor()
            });
        },
        remove: function (index) {
            this.items.splice(index, 1);
        }
    },
    created: function () {
        this.add();
        this.add();
    }
});



var app3 = new Vue({
    el: '#app3',
    data: {
        leds: [
            { state: false },
            { state: true },
            { state: false },
            { state: false },
            { state: true },
            { state: true },
            { state: false },
            { state: false },
        ],
        color: '#ff0000',
    },
    computed: {
        int: {
            get: function () {
                var r = 0;
                for (var i = 0; i < this.leds.length; i++) {
                    r += this.leds[i].state ? Math.pow(2, i) : 0;
                }
                return r;
            },
            set: function (v) {
                var bin = toBin(v);
                for (var i = 0; i < this.leds.length; i++) {
                    this.leds[this.leds.length - i - 1].state = bin[i] == "1";
                }
            }
        },
        hex: {
            get: function () { return toHex(this.int); },
            set: function (v) { this.int = parseInt(v, 16); }
        },
        bin: {
            get: function () { return toBin(this.int); },
            set: function (v) { this.int = parseInt(v, 2); }
        }
    },
    methods: {
        lightSwitch: function (index) {
            this.leds[index].state = !this.leds[index].state;
        }
    }
});