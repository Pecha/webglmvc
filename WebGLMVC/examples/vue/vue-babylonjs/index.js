﻿Vue.use(window.VueBabylonjs);



Vue.component('text-plane', Vue.extend({
    props: ['text', 'color'],
    data: function () {
        return {
            text: this.text,
            color: this.color
        }
    },
    methods: {
        initPlane: function (event) {
            var BABYLON = window.VueBabylonjs.BABYLON;
            var scene = event.scene;
            var entity = event.entity;
            entity.material = new BABYLON.StandardMaterial("material", scene);
            entity.material.backFaceCulling = false;
            entity.material.emissiveColor = new BABYLON.Color3(1, 1, 1);
            this.update = function () {
                var dynamicTexture = new BABYLON.DynamicTexture("dyn", { width: 512, height: 512 }, scene);
                dynamicTexture.drawText(this.text, (256 - ((this.text.length * 40) / 2)), 256, "bold 70px monospace", this.color || "#000000", null, true, true);
                dynamicTexture.hasAlpha = true;
                entity.material.diffuseTexture = dynamicTexture;
            }
            this.update();
        },
    },
    watch: {
        text: function () { if (this.update) { this.update(); } },
        color: function () { if (this.update) { this.update(); } }
    },
    template: '<Plane @entity="initPlane"></Plane>'
}));



var app1 = new Vue({
    el: '#app1',
    data: {
        text: 'Hello world!',
        diffuseColor: '#dd3300'
    }
});



var app2 = new Vue({
    el: '#app2',
    data: {
        items: [],
    },
    methods: {
        add: function () {
            this.items.push({
                size: { x: 1, y: 1, z: 1 },
                position: { x: 0, y: 0, z: 0 },
                color: getRandomColor()
            });
        },
        remove: function (index) {
            this.items.splice(index, 1);
        }
    },
    created: function () {
        this.add();
        this.add();
    }
});



var app3 = new Vue({
    el: '#app3',
    data: {
        leds: [
            { state: false },
            { state: true },
            { state: false },
            { state: false },
            { state: true },
            { state: true },
            { state: false },
            { state: false },
        ],
        color: '#ff0000',
    },
    computed: {
        int: {
            get: function () {
                var r = 0;
                for (var i = 0; i < this.leds.length; i++) {
                    r += this.leds[i].state ? Math.pow(2, i) : 0;
                }
                return r;
            },
            set: function (v) {
                var bin = toBin(v);
                for (var i = 0; i < this.leds.length; i++) {
                    this.leds[this.leds.length - i - 1].state = bin[i] == "1";
                }
            }
        },
        hex: {
            get: function () { return toHex(this.int); },
            set: function (v) { this.int = parseInt(v, 16); }
        },
        bin: {
            get: function () { return toBin(this.int); },
            set: function (v) { this.int = parseInt(v, 2); }
        }
    },
    methods: {
        initButton: function (event, index) {
            event.scene.cameras[0].radius = 15;
            var that = this;
            var BABYLON = window.VueBabylonjs.BABYLON;
            var entity = event.entity;
            entity.actionManager = new BABYLON.ActionManager(event.scene);
            entity.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger, function () { that.lightSwitch(index); }));
        },
        lightSwitch: function (index) {
            this.leds[index].state = !this.leds[index].state;
        }
    }
});