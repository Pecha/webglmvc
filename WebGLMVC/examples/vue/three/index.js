﻿run(function (scene) {

    var data = {};

    // načtení fontu
    var loader = new THREE.FontLoader();
    loader.load('../../../libs/threejs/three.helvetiker.json', function (font) {

        // text ve 3D scéně
        var text = new THREE.Mesh(
            new THREE.TextGeometry("", {
                font: font,
                size: 2,
                height: 0.5,
                curveSegments: 16
            }),
            new THREE.MeshStandardMaterial({})
        );

        // propojení textu a datového modelu aplikace pomocí mapy
        new MW.TwoWayMap({
            end1: data,
            map1: MAP.TEXT.to3DModel(font),
            end2: text,
            map2: MAP.TEXT.toDataModel,
            data1: {
                text: 'Hello world!',
                diffuseColor: '#dd3300',
                emissiveColor: '#ee9966',
            }
        });
        
        scene.add(text);

        // Vue aplikace
        var app1 = new Vue({
            el: '#app1',
            data: data
        });

    });

}, {
    canvas: 'canvas1',
    output: 'code1'
});



run(function (scene) {
    
    var group = new THREE.Group();
    scene.add(group);

    // funcke pro vytvoření 3D kostky
    function createCube(index) {
        var data = {};
        var cube = new THREE.Mesh(
            new THREE.BoxGeometry(),
            new THREE.MeshBasicMaterial({ })
        );
        group.add(cube);

        // propojení kostky s datovým modelem
        new MW.TwoWayMap({
            end1: data,
            map1: MAP.CUBE.to3DModel,
            end2: cube,
            map2: MAP.CUBE.toDataModel,
            data1: {
                color: getRandomColor(),
                element: cube,
                index: index
            }
        });

        group.position.x = - index;
        cube.position.x = (index * 2);
        return data;
    }

    // Vue aplikace
    var app2 = new Vue({
        el: '#app2',
        data: {
            items: [
                createCube(0),
                createCube(1)
            ],
        },
        methods: {
            // metoda pro přidání kostky
            add: function () {
                this.items.push(createCube(this.items.length));
            },
            // metoda pro smazání kostky
            remove: function (index) {
                group.remove(this.items[index].element);
                this.items.splice(index, 1);
                for (var i = index; i < this.items.length; i++) {
                    this.items[i].index = i;
                    this.items[i].element.position.x = this.items[i].element.position.x -2;
                }
                group.position.x = - (this.items.length - 1);
            }
        }
    });

}, {
    canvas: 'canvas2',
    output: 'code2'
});



run(function (scene) {

    // "Vlákno" 3D scény
    window.sceneWorker = new MW.SyncWorker(function (worker) {

        // vytvoření klikatelných objektů pomocí RayCaster
        var clickable = setupClickable(scene);

        // načtení fontu
        var loader = new THREE.FontLoader();
        loader.load('../../../libs/threejs/three.helvetiker.json', function (font) {

            var leds = [], lights = [], number = 0;

            // základní deska
            var ledBoard = new THREE.Group();
            ledBoard.rotation.x = (Math.PI / 2) * 0.8;
            scene.add(ledBoard);
            var boardMaterial = new THREE.MeshStandardMaterial({});
            boardMaterial.color = new THREE.Color(0.05, 0.4, 0.05);
            var board = new THREE.Mesh(new THREE.BoxGeometry(16, 0.1, 6), boardMaterial);
            ledBoard.add(board);

            // Zlatý text
            function GoldText(text, size, z) {
                var textMesh = new THREE.Mesh(
                    new THREE.TextGeometry(text, {
                        font: font,
                        size: size,
                        height: 0.1,
                        curveSegments: 16
                    }),
                    new THREE.MeshStandardMaterial({})
                );
                textMesh.material.color = new THREE.Color(0.8, 0.6, 0);
                textMesh.position.z = z;
                textMesh.position.x = - ((text.length / 2) * size * 0.65);
                textMesh.rotation.x = -(Math.PI / 2);
                this.mesh = textMesh;
            }
            ledBoard.add((new GoldText("LED Diody (8 bitu)", 0.8, -1.5)).mesh);

            // materiály pro LED diodu
            var ledOffMaterial = new THREE.MeshStandardMaterial({});
            ledOffMaterial.color = new THREE.Color(0.4, 0.4, 0.4);
            ledOffMaterial.emissive = new THREE.Color(0.6, 0.6, 0.6);
            ledOffMaterial.opacity = 0.8;
            ledOffMaterial.depthWrite = false;
            ledOffMaterial.transparent = true;
            var ledOnMaterial = new THREE.MeshStandardMaterial({});
            ledOnMaterial.opacity = 0.8;
            ledOnMaterial.depthWrite = false;
            ledOnMaterial.transparent = true;
            ledOnMaterial.roughness = 0.4;

            // LED Dioda
            function Led(index) {
                var led = new THREE.Group();
                var sphere = new THREE.Mesh(new THREE.SphereGeometry(0.4, 16, 16), ledOffMaterial);
                sphere.position.y = 0.86;

                var light1 = new THREE.SpotLight(0x000000);
                light1.position.y = 3.5;
                light1.target = led;
                light1.decay = 2;
                light1.angle = (Math.PI / 6);
                led.add(light1);
                lights.push(light1);
                var light2 = new THREE.SpotLight(0x000000);
                light2.position.y = 0;
                light2.target = sphere;
                light2.decay = 2;
                light2.angle = 1.2;
                led.add(light2);
                lights.push(light2);

                var cylinder = new THREE.Mesh(new THREE.CylinderGeometry(0.4, 0.4, 0.9, 16, 1), ledOffMaterial);
                cylinder.position.y = 0.46;
                var bottom = new THREE.Mesh(new THREE.CylinderGeometry(0.45, 0.45, 0.2, 16, 1), ledOffMaterial);
                bottom.position.y = 0.11;
                led.add(sphere);
                led.add(cylinder);
                led.add(bottom);

                this.switch = function (state) {
                    if (state) {
                        light1.intensity = 2.5;
                        light2.intensity = 2.5;
                        sphere.material = ledOnMaterial;
                        cylinder.material = ledOnMaterial;
                        bottom.material = ledOnMaterial;
                    } else {
                        light1.intensity = 0;
                        light2.intensity = 0;
                        sphere.material = ledOffMaterial;
                        cylinder.material = ledOffMaterial;
                        bottom.material = ledOffMaterial;
                    }
                }
                this.mesh = led;
            }

            // materiály pro tlačítko
            var buttonMaterial = new THREE.MeshStandardMaterial({});
            buttonMaterial.color = new THREE.Color(0.5, 0.5, 0.8);
            var bottomMaterial = new THREE.MeshStandardMaterial({});
            bottomMaterial.color = new THREE.Color(0.1, 0.1, 0.1);

            // Tlačítko
            function Button(index, switchableLed) {
                var button = new THREE.Group();
                var cylinder = new THREE.Mesh(new THREE.CylinderGeometry(0.4, 0.4, 0.8, 16, 1), buttonMaterial);
                cylinder.position.y = 0.51;
                var bottom = new THREE.Mesh(new THREE.BoxGeometry(1, 0.5, 1), buttonMaterial);
                bottom.position.y = 0.26;
                cylinder.material = buttonMaterial;
                bottom.material = bottomMaterial;
                button.add(cylinder);
                button.add(bottom);
                button.position.z = 2;

                cylinder.onclick = function () {
                    switchableLed.switch(!switchableLed.state);
                    number = (number * 1) + (Math.pow(2, index) * (switchableLed.state ? 1 : -1));
                    worker.postMessage({ type: "number", value: number });
                }
                clickable.push(cylinder);

                this.switch = function (state) {
                    cylinder.scale = state ? 0.5 : 1;
                }
                this.mesh = button;
            }

            // LED Dioda s tlačítkem
            function SwitchableLed(index) {
                var group = new THREE.Group();
                var led = new Led(index);
                var button = new Button(index, this);
                var text = new GoldText((Math.pow(2, index)).toString(), 0.5, 1.2);
                group.add(led.mesh);
                group.add(button.mesh);
                group.add(text.mesh);
                group.position.x = ((((8 - index) - (8 / 2)) * 2) - 1);
                ledBoard.add(group);

                this.switch = function (state) {
                    led.switch(state);
                    button.switch(state);
                    this.state = state == true;
                }
                this.state = false;
            }

            // inicializace LED Diod s tlačítky
            for (var i = 0; i < 8; i++) {
                leds.push(new SwitchableLed(i));
            }

            // funkce pro nastavení čísla
            function setNumber(value) {
                number = value;
                var bin = toBin(number);
                for (var i = 0; i < leds.length; i++) {
                    leds[leds.length - i - 1].switch(bin[i] == "1");
                }
            }

            // funkce pro nastavení barvy světla
            function setColor(value) {
                ledOnMaterial.color.setStyle(value);
                ledOnMaterial.color.multiplyScalar(0.9);
                ledOnMaterial.emissive.setStyle(value);
                ledOnMaterial.emissive.multiplyScalar(1.1);
                for (var i = 0; i < lights.length; i++) {
                    lights[i].color.setStyle(value);
                }
            }

            // zpracování přijatých zpráv
            worker.addEventListener("message", function (data) {
                switch (data.type) {
                    case "init":
                        setColor(data.color);
                        setNumber(data.number * 1);
                        break;
                    case "number":
                        setNumber(data.value * 1);
                        break;
                    case "color":
                        setColor(data.value);
                        break;
                    default: break;
                }
            });
            worker.postMessage({ type: "sceneReady" });

        });

    }, true);

}, {
    canvas: 'canvas3',
    output: 'code3'
});



run(function () {

    // "Vlákno" Vue aplikace
    window.appWorker = new MW.SyncWorker(function (worker) {

        // výchozí stav datového modelu
        var data = {
            int: 50,
            color: '#ff0000'
        }

        // zpracování přijatých zpráv
        worker.addEventListener("message", function (message) {
            switch (message.type) {
                case "number":
                    data.int = message.value;
                    break;
                case "sceneReady":
                    worker.postMessage({ type: "init", number: data.int, color: data.color });
                    break;
                default: break;
            }
        });

        // Vue aplikace
        var app3 = new Vue({
            el: '#app3',
            data: data,
            // dopočítáváné vlastnosti pro hexadecimální a binární zápis
            computed: {
                hex: {
                    get: function () { return toHex(this.int); },
                    set: function (v) { this.int = parseInt(v, 16); }
                },
                bin: {
                    get: function () { return toBin(this.int); },
                    set: function (v) { this.int = parseInt(v, 2); }
                }
            },
            // sledování změn čísla a barvy
            watch: {
                int: function (v) {
                    worker.postMessage({ type: "number", value: v });
                },
                color: function (v) {
                    worker.postMessage({ type: "color", value: v });
                }
            }
        });
    }, true);

    // samotné propojení zasílání zpráv
    window.appWorker.addEventListener("message", window.sceneWorker.postMessage);
    window.sceneWorker.addEventListener("message", window.appWorker.postMessage);

}, {
    output: 'code4',
    before: '// VUE Aplikace v rámci SyncWorker\r\n'
});