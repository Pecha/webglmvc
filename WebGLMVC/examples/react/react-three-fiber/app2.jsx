﻿import React, { useRef } from 'react'
import ReactDOM from 'react-dom'
import * as THREE from 'three'
import { Canvas, useThree, useRender, extend } from 'react-three-fiber'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'

extend({ OrbitControls })

const Controls = (props) => {
    const { camera, gl } = useThree();
    const controls = useRef();
    useRender(({ camera }) => {
        controls.current && controls.current.update();
        camera.updateMatrixWorld();
    })
    return <orbitControls ref={controls} args={[camera, gl.domElement]} {...props} />
}

const BoxThree = ({ instance, index }) => {
    const { position, size, color } = instance;
    return (
        <group position={new THREE.Vector3(index * 2, 0, 0)}>
            <mesh position={new THREE.Vector3(position.x, position.y, position.z)}>
                <boxBufferGeometry attach="geometry" args={[size.x, size.y, size.z]} />
                <meshStandardMaterial attach="material" color={new THREE.Color(color)} />
            </mesh>
        </group>
    );
}

const BoxControl = ({ instance, index, onChange, remove }) => {
    const { position, size, color } = instance;
    return (
        <li>
            <div className="uk-margin-small-top uk-box-shadow-small uk-padding-small">
                <div className="uk-grid-small" uk-grid="">
                    <div className="uk-width-auto">
                        <label className="uk-form-label">Objekt {index + 1}</label>
                        <div className="uk-form-controls">
                            <input type="color" value={color} uk-tooltip="Barva" onChange={onChange((box, value) => box.color = value)} />
                            <span style={{ color: color }}> {color} </span>
                        </div>
                    </div>
                    <div className="uk-width-expand">
                        <div className="uk-child-width-1-6 uk-grid-small uk-text-center" uk-grid="">
                            <div>
                                <label className="uk-form-label">↔ W {Math.floor(size.x * 100)}%</label>
                                <div className="uk-form-controls">
                                    <input type="range" value={size.x} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.x = value)} />
                                </div>
                            </div>
                            <div>
                                <label className="uk-form-label">↕ H {Math.floor(size.y * 100)}%</label>
                                <div className="uk-form-controls">
                                    <input type="range" value={size.y} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.y = value)} />
                                </div>
                            </div>
                            <div>
                                <label className="uk-form-label">⤢ L {Math.floor(size.z * 100)}%</label>
                                <div className="uk-form-controls">
                                    <input type="range" value={size.z} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.z = value)} />
                                </div>
                            </div>
                            <div>
                                <label className="uk-form-label">→ X {position.x}</label>
                                <div className="uk-form-controls">
                                    <input type="range" value={position.x} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.x = value)} />
                                </div>
                            </div>
                            <div>
                                <label className="uk-form-label">↑ Y {position.y}</label>
                                <div className="uk-form-controls">
                                    <input type="range" value={position.y} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.y = value)} />
                                </div>
                            </div>
                            <div>
                                <label className="uk-form-label">↗ Z {position.z}</label>
                                <div className="uk-form-controls">
                                    <input type="range" value={position.z} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.z = value)} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="uk-width-auto uk-text-right">
                        <span className="uk-icon-link" uk-tooltip="Smazat" uk-icon="icon: close; ratio: 1.5" onClick={() => remove()}></span>
                    </div>
                </div>
            </div>
        </li>
    );
}

class ExampleApp2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            boxes: [
                {
                    position: { x: 0, y: 0, z: 0 },
                    size: { x: 1, y: 1, z: 1 },
                    color: getRandomColor()
                }
            ]
        };
    }

    changeState(index) {
        var that = this;
        return (boxStateHandler) => {
            return (e) => {
                var value = e.target.value;
                return that.setState((state) => {
                    boxStateHandler(state.boxes[index], value);
                    return state;
                });
            }
        }
    }

    remove(index) {
        var that = this;
        return () => {
            return that.setState((state) => {
                state.boxes.splice(index, 1);
                return state;
            });
        }
    }

    add() {
        this.setState((state) => {
            state.boxes.push({
                position: { x: 0, y: 0, z: 0 },
                size: { x: 1, y: 1, z: 1 },
                color: getRandomColor()
            });
            return state;
        });
    };

    render() {
        return (
            <div>
                <div className="fixed-ratio">
                    <Canvas camera={{ position: [0, 0, 5] }}>
                        <Controls />
                        <pointLight position={[2, 2, 5]} />
                        <group position={new THREE.Vector3((- this.state.boxes.length + 1), 0, 0)}>
                            {this.state.boxes.map((box, index) => (
                                <BoxThree instance={box} index={index} key={index} />
                            ))}
                        </group>
                    </Canvas>
                </div>
                <form className="uk-form-stacked">
                    <ul className="uk-list">
                        {this.state.boxes.map((box, index) => (
                            <BoxControl instance={box} index={index} key={index} onChange={this.changeState(index)} remove={this.remove(index)} />
                        ))}
                    </ul>
                    <div onClick={(e) => this.add()} className="uk-link-reset"><span uk-icon="plus"></span> Přidat nový objekt</div>
                </form>
            </div>
        );
    }
}

ReactDOM.render(<ExampleApp2 />, document.getElementById('app2'));