﻿import React, { Suspense, useMemo, useRef } from 'react'
import ReactDOM from 'react-dom'
import * as THREE from 'three'
import { Canvas, useLoader, useUpdate, useThree, useRender, extend } from 'react-three-fiber'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'



const Text = ({ children, vAlign = 'center', hAlign = 'center', size = 1, diffuseColor = '#dd3300', emissiveColor = '#ee9966', ...props }) => {
  const font = useLoader(THREE.FontLoader, '../../../libs/threejs/three.helvetiker.json')
  const config = useMemo(
    () => ({ font, size: 20, height: 4, curveSegments: 32 }),
    [font]
  )
  const mesh = useUpdate(
    self => {
      const size = new THREE.Vector3()
      self.geometry.computeBoundingBox()
      self.geometry.boundingBox.getSize(size)
      self.position.x = hAlign === 'center' ? -size.x / 2 : hAlign === 'right' ? 0 : -size.x
      self.position.y = vAlign === 'center' ? -size.y / 2 : vAlign === 'top' ? 0 : -size.y
	  self.material.color.setStyle(diffuseColor);
	  self.material.emissive.setStyle(emissiveColor);
    },
    [children]
  )
  return (
    <group {...props} scale={[0.1 * size, 0.1 * size, 0.1]}>
      <mesh ref={mesh}>
        <textGeometry attach="geometry" args={[children, config]} />
        <meshStandardMaterial attach="material" color={diffuseColor} emissive={emissiveColor} emissiveIntensity={0.4} />
      </mesh>
    </group>
  )
}



extend({ OrbitControls })

const Controls = (props) => {
    const { camera, gl } = useThree();
    const controls = useRef();
    useRender(({ camera }) => {
        controls.current && controls.current.update();
        camera.updateMatrixWorld();
    })
    return <orbitControls ref={controls} args={[camera, gl.domElement]} {...props} />
}



class ExampleApp1 extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			text: 'Hello world!',
			diffuseColor: '#dd3300',
			emissiveColor: '#ee9966'
		};
	}

	renderScene() {
		return (
			<div className="fixed-ratio">
				<Canvas camera={{ position: [0, 0, 5] }}>
					<Controls />
					<pointLight position={[2, 2, 5]} />
					<Suspense fallback={null}>
						<Text hAlign="center" position={[0, 0, 0]} children={this.state.text} diffuseColor={this.state.diffuseColor} emissiveColor={this.state.emissiveColor} />
					</Suspense>
				</Canvas>
			</div>
		);
	}


	renderControls() {
		return (
			<form className="uk-form-stacked">
				<div className="uk-margin-small-top">
					<label className="uk-form-label">Text</label>
					<div className="uk-form-controls">
						<input type="text" value={this.state.text} className="uk-input" onChange={(e) => this.setState({ text: e.target.value })} />
					</div>
				</div>
				<div className="uk-margin-small-top">
					<div className="uk-child-width-1-2 uk-grid-small" uk-grid="">
						<div>
							<label className="uk-form-label">Základní barva</label>
							<div className="uk-form-controls">
								<input type="color" value={this.state.diffuseColor} onChange={(e) => this.setState({ diffuseColor: e.target.value })} />
								<span style={{ color: this.state.diffuseColor }}>{this.state.diffuseColor}</span>
							</div>
						</div>
						<div>
							<label className="uk-form-label">Vyzařovaná barva</label>
							<div className="uk-form-controls">
								<input type="color" value={this.state.emissiveColor} onChange={(e) => this.setState({ emissiveColor: e.target.value })} />
								<span style={{ color: this.state.emissiveColor }}>{this.state.emissiveColor}</span>
							</div>
						</div>
					</div>
				</div>
			</form>
		);
	}

	render() {
		return (
			<div>
				{this.renderScene()}
				{this.renderControls()}
			</div>
		);
	}
}

ReactDOM.render(<ExampleApp1 />, document.getElementById('app1'));