﻿run(function () {

    class ExampleApp1 extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                text: 'Hello world!',
                diffuseColor: '#dd3300',
                specularColor: '#ee9966'
            };
        }

        renderScene() {
            return (
                <x3d is="x3d">
                    <scene is="x3d">
                        <shape is="x3d">
                            <appearance is="x3d">
                                <material diffusecolor={this.state.diffuseColor} specularcolor={this.state.specularColor} is="x3d"></material>
                            </appearance>
                            <text solid="false" string={this.state.text} is="x3d">
                                <fontstyle
                                    family="TYPEWRITER"
                                    justify="MIDDLE"
                                    quality={200}
                                    size={2}
                                    is="x3d">
                                </fontstyle>
                            </text>
                        </shape>
                    </scene>
                </x3d>
            );
        }

        renderControls() {
            return (
                <form className="uk-form-stacked">
                    <div className="uk-margin-small-top">
                        <label className="uk-form-label">Text</label>
                        <div className="uk-form-controls">
                            <input type="text" value={this.state.text} className="uk-input" onChange={(e) => this.setState({ text: e.target.value })} />
                        </div>
                    </div>
                    <div className="uk-margin-small-top">
                        <div className="uk-child-width-1-2 uk-grid-small" uk-grid="">
                            <div>
                                <label className="uk-form-label">Základní barva</label>
                                <div className="uk-form-controls">
                                    <input type="color" value={this.state.diffuseColor} onChange={(e) => this.setState({ diffuseColor: e.target.value })} />
                                    <span style={{ color: this.state.diffuseColor }}>{this.state.diffuseColor}</span>
                                </div>
                            </div>
                            <div>
                                <label className="uk-form-label">Barva lesku</label>
                                <div className="uk-form-controls">
                                    <input type="color" value={this.state.specularColor} onChange={(e) => this.setState({ specularColor: e.target.value })} />
                                    <span style={{ color: this.state.specularColor }}>{this.state.specularColor}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            );
        }

        render() {
            return (
                <div>
                    {this.renderScene()}
                    {this.renderControls()}
                </div>
            );
        }
    }

    ReactDOM.render(<ExampleApp1 />, document.getElementById('app1'));
    x3dom.reload();

}, {
    before: '// React JSX zkompilovaný pomocí Babel.js\r\n',
    output: 'code1'
});



run(function () {

    const BoxX3D = ({ instance, index }) => {
        const { position, size, color } = instance;
        return (
            <transform translation={(index * 2) + ' 0 0'} is="x3d">
                <transform translation={position.x + ' ' + position.y + ' ' + position.z} is="x3d">
                    <shape is="x3d">
                        <appearance is="x3d">
                            <material diffusecolor={color} is="x3d"></material>
                        </appearance>
                        <box size={size.x + ' ' + size.y + ' ' + size.z} usegeocache="false" is="x3d"></box>
                    </shape>
                </transform>
            </transform>
        );
    }

    const BoxControl = ({ instance, index, onChange, remove }) => {
        const { position, size, color } = instance;
        return (
            <li>
                <div className="uk-margin-small-top uk-box-shadow-small uk-padding-small">
                    <div className="uk-grid-small" uk-grid="">
                        <div className="uk-width-auto">
                            <label className="uk-form-label">Objekt {index + 1}</label>
                            <div className="uk-form-controls">
                                <input type="color" value={color} uk-tooltip="Barva" onChange={onChange((box, value) => box.color = value)} />
                                <span style={{ color: color }}> { color } </span>
                            </div>
                        </div>
                        <div className="uk-width-expand">
                            <div className="uk-child-width-1-6 uk-grid-small uk-text-center" uk-grid="">
                                <div>
                                    <label className="uk-form-label">↔ W { Math.floor(size.x * 100) }%</label>
                                    <div className="uk-form-controls">
                                        <input type="range" value={size.x} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.x = value)} />
                                    </div>
                                </div>
                                <div>
                                    <label className="uk-form-label">↕ H { Math.floor(size.y * 100) }%</label>
                                    <div className="uk-form-controls">
                                        <input type="range" value={size.y} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.y = value)} />
                                    </div>
                                </div>
                                <div>
                                    <label className="uk-form-label">⤢ L { Math.floor(size.z * 100) }%</label>
                                    <div className="uk-form-controls">
                                        <input type="range" value={size.z} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.z = value)} />
                                    </div>
                                </div>
                                <div>
                                    <label className="uk-form-label">→ X { position.x }</label>
                                    <div className="uk-form-controls">
                                        <input type="range" value={position.x} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.x = value)} />
                                    </div>
                                </div>
                                <div>
                                    <label className="uk-form-label">↑ Y { position.y }</label>
                                    <div className="uk-form-controls">
                                        <input type="range" value={position.y} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.y = value)} />
                                    </div>
                                </div>
                                <div>
                                    <label className="uk-form-label">↗ Z { position.z }</label>
                                    <div className="uk-form-controls">
                                        <input type="range" value={position.z} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.z = value)} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="uk-width-auto uk-text-right">
                            <a className="uk-icon-link" uk-tooltip="Smazat" uk-icon="icon: close; ratio: 1.5" onClick={() => remove()}></a>
                        </div>
                    </div>
                </div>
            </li>
        );
    }


    class ExampleApp2 extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                boxes: []
            };
        }

        changeState(index) {
            var that = this;
            return (boxStateHandler) => {
                return (e) => {
                    var value = e.target.value;
                    return that.setState((state) => {
                        boxStateHandler(state.boxes[index], value);
                        return state;
                    });
                }
            }
        }

        componentDidMount() {
            this.add();
            this.add();
        }

        remove(index) {
            var that = this;
            return () => {
                return that.setState((state) => {
                    state.boxes.splice(index, 1);
                    return state;
                });
            }
        }

        add() {
            this.setState((state) => {
                state.boxes.push({
                    position: { x: 0, y: 0, z: 0 },
                    size: { x: 1, y: 1, z: 1 },
                    color: getRandomColor()
                });
                return state;
            });
        };

        render() {
            return (
                <div>
                    <x3d is="x3d">
                        <scene is="x3d">
                            <transform translation={( - this.state.boxes.length + 1) + ' 0 0'} is="x3d">
                                {this.state.boxes.map((box, index) => (
                                    <BoxX3D instance={box} index={index} key={index} />
                                ))}
                            </transform>
                        </scene>
                    </x3d>
                    <form className="uk-form-stacked">
                        <ul className="uk-list">
                            {this.state.boxes.map((box, index) => (
                                <BoxControl instance={box} index={index} key={index} onChange={this.changeState(index)} remove={this.remove(index)} />
                            ))}
                        </ul>
                        <a onClick={(e) => this.add()} className="uk-link-reset"><span uk-icon="plus"></span> Přidat nový objekt</a>
                    </form>
                </div>
            );
        }
    }

    ReactDOM.render(<ExampleApp2 />, document.getElementById('app2'));
    x3dom.reload();

}, {
    before: '// React JSX zkompilovaný pomocí Babel.js\r\n',
    output: 'code2'
});



run(function () {

    const Seat = ({ row, column, enabled, picker }) => {
        const click = 'window.SeatPicker(' + row + ',' + column + ')()';
        return (
            <transform translation={(column * 1.4) + ' ' + (row * -0.5) + ' ' + (row * 2.5)} is="x3d">
                <shape is="x3d" onclick={click}>
                    <appearance is="x3d">
                        <material diffusecolor={enabled ? '#0099ff' : '#ff9900'} is="x3d"></material>
                    </appearance>
                    <box size="0.6 0.5 0.5" usegeocache="false" is="x3d"></box>
                </shape>
                <transform translation="0.4 0.15 0" is="x3d">
                    <shape is="x3d" onclick={click}>
                        <appearance is="x3d">
                            <material diffusecolor={enabled ? '#0055ee' : '#ee5500'} is="x3d"></material>
                        </appearance>
                        <box size="0.2 0.8 0.6" usegeocache="false" is="x3d"></box>
                    </shape>
                </transform>
                <transform translation="-0.4 0.15 0" is="x3d">
                    <shape is="x3d" onclick={click}>
                        <appearance is="x3d">
                            <material diffusecolor={enabled ? '#0055ee' : '#ee5500'} is="x3d"></material>
                        </appearance>
                        <box size="0.2 0.8 0.6" usegeocache="false" is="x3d"></box>
                    </shape>
                </transform>
                <transform translation="0 0.35 -0.25" is="x3d">
                    <shape is="x3d" onclick={click}>
                        <appearance is="x3d">
                            <material diffusecolor={enabled ? '#0077ff' : '#ff7700'} is="x3d"></material>
                        </appearance>
                        <box size="0.8 1.2 0.1" usegeocache="false" is="x3d"></box>
                    </shape>
                </transform>
            </transform>
        );
    }

    const SeatButton = ({ row, column, enabled, picker }) => {
        const click = () => picker(row, column);
        return (
            <input type='button' className={enabled ? 'uk-button seat-free' : 'uk-button seat-taken'} onClick={click} value={'S ' + (column + 1)} />
        );
    }

    class ExampleApp3 extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                seats: [
                    [true, true, true, true, true, true, true],
                    [true, true, true, true, true, true, true],
                    [true, true, true, true, true, true, true],
                    [true, true, true, true, true, true, true]
                ],
                free: 28,
                taken: 0
            };
        }

        getSeatPicker() {
            var that = this;
            return (row, column) => {
                that.setState((state) => {
                    state.seats[row][column] = !state.seats[row][column];
                    state.seats[row][column] ? (state.free++ , state.taken--) : (state.taken++ , state.free--);
                    return state;
                });
            }
        }

        componentDidMount() {
            window.SeatPicker = this.getSeatPicker();
        }

        render() {
            return (
                <div>
                    <x3d is="x3d">
                        <scene is="x3d">
                            <transform rotation={'1 0 0 ' + Math.PI / 6} is="x3d">
                                <transform translation={((-this.state.seats[0].length / 2) - 0.5) + ' 0 ' + (-this.state.seats.length * 1.2)} is="x3d">
                                    {this.state.seats.map((row, rowIndex) => (
                                        row.map((enabled, colIndex) => (
                                            <Seat row={rowIndex} column={colIndex} enabled={enabled} key={rowIndex + 'x' + colIndex} />
                                        ))
                                    ))}
                                </transform>
                            </transform>
                        </scene>
                    </x3d>
                    <form className="uk-form-stacked">
                        <h3>Vyberte sedadlo</h3>
                        <table className="uk-table uk-table-divider">
                            <caption>Obsazeno je {this.state.taken}, zbývá {this.state.free} volných míst</caption>
                            <tbody>
                                {this.state.seats.map((row, rowIndex) => (
                                    <tr key={rowIndex}>
                                        <td>Řada {rowIndex + 1}</td>
                                        {row.map((enabled, colIndex) => (
                                            <td key={rowIndex + 'x' + colIndex}>
                                                <SeatButton row={rowIndex} column={colIndex} enabled={enabled} picker={this.getSeatPicker()} />
                                            </td>
                                        ))}
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </form>
                </div>
            );
        }
    }

    ReactDOM.render(<ExampleApp3 />, document.getElementById('app3'));
    x3dom.reload();
}, {
    before: '// React JSX zkompilovaný pomocí Babel.js\r\n',
    output: 'code3'
});