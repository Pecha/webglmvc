﻿import React from 'react';
import ReactDOM from 'react-dom';
import { Engine, Scene, ArcRotateCamera, HemisphericLight } from 'react-babylonjs'
import { Vector3, Color3, Color4 } from '@babylonjs/core'
import { Control } from '@babylonjs/gui'

class ExampleApp1 extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			text: 'Hello world!',
			diffuseColor: '#dd3300',
			outlineColor: '#ee9966'
		};
	}
	
	onSceneMount(e) {
		const { scene } = e;
		scene.clearColor = new Color4(0.133, 0.133, 0.133, 1);
	}

	renderScene() {
		return (
			<div className="fixed-ratio">
				<Engine antialias adaptToDeviceRatio>
				  <Scene onSceneMount={this.onSceneMount}>
					<ArcRotateCamera name="camera1" radius={7} beta={Math.PI / 2} alpha={-(Math.PI / 2)} target={Vector3.Zero()} minZ={0.001} wheelPrecision={30} />
					<HemisphericLight name="light1" intensity={0.7} direction={Vector3.Up()} />
					<plane name='plane1' width={20} height={20}>
						<advancedDynamicTexture name='texture1' height={2048} width={2048} createForParentMesh>
							<textBlock
								name='text1'
								text={this.state.text}
								color={this.state.diffuseColor}
								fontSize={200}
								outlineWidth={5}
								outlineColor={this.state.outlineColor}
							/>
						</advancedDynamicTexture>
					</plane>
				  </Scene>
				</Engine>
			</div>
		);
	}


	renderControls() {
		return (
			<form className="uk-form-stacked">
				<div className="uk-margin-small-top">
					<label className="uk-form-label">Text</label>
					<div className="uk-form-controls">
						<input type="text" value={this.state.text} className="uk-input" onChange={(e) => this.setState({ text: e.target.value })} />
					</div>
				</div>
				<div className="uk-margin-small-top">
					<div className="uk-child-width-1-2 uk-grid-small" uk-grid="">
						<div>
							<label className="uk-form-label">Základní barva</label>
							<div className="uk-form-controls">
								<input type="color" value={this.state.diffuseColor} onChange={(e) => this.setState({ diffuseColor: e.target.value })} />
								<span style={{ color: this.state.diffuseColor }}>{this.state.diffuseColor}</span>
							</div>
						</div>
						<div>
							<label className="uk-form-label">Barva okraje</label>
							<div className="uk-form-controls">
								<input type="color" value={this.state.outlineColor} onChange={(e) => this.setState({ outlineColor: e.target.value })} />
								<span style={{ color: this.state.outlineColor }}>{this.state.outlineColor}</span>
							</div>
						</div>
					</div>
				</div>
			</form>
		);
	}

	render() {
		return (
			<div>
				{this.renderScene()}
				{this.renderControls()}
			</div>
		);
	}
}


ReactDOM.render(<ExampleApp1 />, document.getElementById('app1'));