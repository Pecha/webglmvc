﻿import React from 'react';
import ReactDOM from 'react-dom';
import { Engine, Scene, ArcRotateCamera, PointLight } from 'react-babylonjs'
import { Vector3, Color3, Color4 } from '@babylonjs/core'

const BoxBabylon = ({ instance, index, total }) => {
    const { position, size, color } = instance;
    return (
	  <box name={ 'box' + index }
		size={1}
		position={new Vector3((position.x + (index * 2) + 1 - total), position.y, position.z)}
		scaling={new Vector3(size.x, size.y, size.z)}>
		<standardMaterial name={'mat' + index} diffuseColor={Color3.FromHexString(color)} />
	  </box>
    );
}

const BoxControl = ({ instance, index, onChange, remove }) => {
    const { position, size, color } = instance;
    return (
        <li>
            <div className="uk-margin-small-top uk-box-shadow-small uk-padding-small">
                <div className="uk-grid-small" uk-grid="">
                    <div className="uk-width-auto">
                        <label className="uk-form-label">Objekt {index + 1}</label>
                        <div className="uk-form-controls">
                            <input type="color" value={color} uk-tooltip="Barva" onChange={onChange((box, value) => box.color = value)} />
                            <span style={{ color: color }}> {color} </span>
                        </div>
                    </div>
                    <div className="uk-width-expand">
                        <div className="uk-child-width-1-6 uk-grid-small uk-text-center" uk-grid="">
                            <div>
                                <label className="uk-form-label">↔ W {Math.floor(size.x * 100)}%</label>
                                <div className="uk-form-controls">
                                    <input type="range" value={size.x} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.x = value)} />
                                </div>
                            </div>
                            <div>
                                <label className="uk-form-label">↕ H {Math.floor(size.y * 100)}%</label>
                                <div className="uk-form-controls">
                                    <input type="range" value={size.y} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.y = value)} />
                                </div>
                            </div>
                            <div>
                                <label className="uk-form-label">⤢ L {Math.floor(size.z * 100)}%</label>
                                <div className="uk-form-controls">
                                    <input type="range" value={size.z} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.z = value)} />
                                </div>
                            </div>
                            <div>
                                <label className="uk-form-label">→ X {position.x}</label>
                                <div className="uk-form-controls">
                                    <input type="range" value={position.x} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.x = value)} />
                                </div>
                            </div>
                            <div>
                                <label className="uk-form-label">↑ Y {position.y}</label>
                                <div className="uk-form-controls">
                                    <input type="range" value={position.y} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.y = value)} />
                                </div>
                            </div>
                            <div>
                                <label className="uk-form-label">↗ Z {position.z}</label>
                                <div className="uk-form-controls">
                                    <input type="range" value={position.z} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.z = value)} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="uk-width-auto uk-text-right">
                        <span className="uk-icon-link" uk-tooltip="Smazat" uk-icon="icon: close; ratio: 1.5" onClick={() => remove()}></span>
                    </div>
                </div>
            </div>
        </li>
    );
}

class ExampleApp2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            boxes: [
                {
                    position: { x: 0, y: 0, z: 0 },
                    size: { x: 1, y: 1, z: 1 },
                    color: getRandomColor()
                }
            ]
        };
    }

    changeState(index) {
        var that = this;
        return (boxStateHandler) => {
            return (e) => {
                var value = e.target.value;
                return that.setState((state) => {
                    boxStateHandler(state.boxes[index], value);
                    return state;
                });
            }
        }
    }

    remove(index) {
        var that = this;
        return () => {
            return that.setState((state) => {
                state.boxes.splice(index, 1);
                return state;
            });
        }
    }

    add() {
        this.setState((state) => {
            state.boxes.push({
                position: { x: 0, y: 0, z: 0 },
                size: { x: 1, y: 1, z: 1 },
                color: getRandomColor()
            });
            return state;
        });
    };

	onSceneMount(e) {
		const { scene } = e;
        scene.clearColor = new Color4(0.133, 0.133, 0.133, 1);
	}

    render() {
        return (
            <div>
                <div className="fixed-ratio">
					<Engine antialias adaptToDeviceRatio>
					  <Scene onSceneMount={this.onSceneMount}>
						<ArcRotateCamera name="camera1" radius={7} beta={Math.PI / 2} alpha={-(Math.PI / 2)} target={Vector3.Zero()} minZ={0.001} wheelPrecision={30} />
                        <PointLight name="light1" position={new Vector3(2,2,-5)} intensity={0.7} />
						{this.state.boxes.map((box, index) => (
							<BoxBabylon instance={box} index={index} total={this.state.boxes.length} key={index} />
						))}
					  </Scene>
					</Engine>
                </div>
                <form className="uk-form-stacked">
                    <ul className="uk-list">
                        {this.state.boxes.map((box, index) => (
                            <BoxControl instance={box} index={index} key={index} onChange={this.changeState(index)} remove={this.remove(index)} />
                        ))}
                    </ul>
                    <div onClick={(e) => this.add()} className="uk-link-reset"><span uk-icon="plus"></span> Přidat nový objekt</div>
                </form>
            </div>
        );
    }
}

ReactDOM.render(<ExampleApp2 />, document.getElementById('app2'));