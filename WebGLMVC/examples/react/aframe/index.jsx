﻿run(function () {

    class ExampleApp1 extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                text: 'Hello world!',
                diffuseColor: '#dd3300'
            };
        }

        renderScene() {
            return (
                <div className="fixed-ratio">
                    <a-scene embedded="">
                        <a-entity position="0 0 0" camera="" look-controls=""></a-entity>
                        <a-text
                            value={this.state.text}
                            color={this.state.diffuseColor}
                            align="center"
                            position="0 0 -0.5">
                        </a-text>
                    </a-scene>
                </div>
            );
        }

        renderControls() {
            return (
                <form className="uk-form-stacked">
                    <div className="uk-margin-small-top">
                        <label className="uk-form-label">Text</label>
                        <div className="uk-form-controls">
                            <input type="text" value={this.state.text} className="uk-input" onChange={(e) => this.setState({ text: e.target.value })} />
                        </div>
                    </div>
                    <div className="uk-margin-small-top">
                        <div className="uk-child-width-1-2 uk-grid-small" uk-grid="">
                            <div>
                                <label className="uk-form-label">Základní barva</label>
                                <div className="uk-form-controls">
                                    <input type="color" value={this.state.diffuseColor} onChange={(e) => this.setState({ diffuseColor: e.target.value })} />
                                    <span style={{ color: this.state.diffuseColor }}>{this.state.diffuseColor}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            );
        }

        render() {
            return (
                <div>
                    {this.renderScene()}
                    {this.renderControls()}
                </div>
            );
        }
    }

    ReactDOM.render(<ExampleApp1 />, document.getElementById('app1'));

}, {
    before: '// React JSX zkompilovaný pomocí Babel.js\r\n',
    output: 'code1'
});



run(function () {

    const BoxAFrame = ({ instance, index }) => {
        const { position, size, color } = instance;
        return (
            <a-entity position={(index * 2) + ' 0 0'}>
                <a-box
                    color={color}
                    position={position.x + ' ' + position.y + ' ' + position.z}
                    width={size.x}
                    height={size.y}
                    depth={size.z}>
                </a-box>
            </a-entity>
        );
    }

    const BoxControl = ({ instance, index, onChange, remove }) => {
        const { position, size, color } = instance;
        return (
            <li>
                <div className="uk-margin-small-top uk-box-shadow-small uk-padding-small">
                    <div className="uk-grid-small" uk-grid="">
                        <div className="uk-width-auto">
                            <label className="uk-form-label">Objekt {index + 1}</label>
                            <div className="uk-form-controls">
                                <input type="color" value={color} uk-tooltip="Barva" onChange={onChange((box, value) => box.color = value)} />
                                <span style={{ color: color }}> { color } </span>
                            </div>
                        </div>
                        <div className="uk-width-expand">
                            <div className="uk-child-width-1-6 uk-grid-small uk-text-center" uk-grid="">
                                <div>
                                    <label className="uk-form-label">↔ W { Math.floor(size.x * 100) }%</label>
                                    <div className="uk-form-controls">
                                        <input type="range" value={size.x} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.x = value)} />
                                    </div>
                                </div>
                                <div>
                                    <label className="uk-form-label">↕ H { Math.floor(size.y * 100) }%</label>
                                    <div className="uk-form-controls">
                                        <input type="range" value={size.y} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.y = value)} />
                                    </div>
                                </div>
                                <div>
                                    <label className="uk-form-label">⤢ L { Math.floor(size.z * 100) }%</label>
                                    <div className="uk-form-controls">
                                        <input type="range" value={size.z} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.z = value)} />
                                    </div>
                                </div>
                                <div>
                                    <label className="uk-form-label">→ X { position.x }</label>
                                    <div className="uk-form-controls">
                                        <input type="range" value={position.x} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.x = value)} />
                                    </div>
                                </div>
                                <div>
                                    <label className="uk-form-label">↑ Y { position.y }</label>
                                    <div className="uk-form-controls">
                                        <input type="range" value={position.y} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.y = value)} />
                                    </div>
                                </div>
                                <div>
                                    <label className="uk-form-label">↗ Z { position.z }</label>
                                    <div className="uk-form-controls">
                                        <input type="range" value={position.z} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.z = value)} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="uk-width-auto uk-text-right">
                            <a className="uk-icon-link" uk-tooltip="Smazat" uk-icon="icon: close; ratio: 1.5" onClick={() => remove()}></a>
                        </div>
                    </div>
                </div>
            </li>
        );
    }


    class ExampleApp2 extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                boxes: []
            };
        }

        changeState(index) {
            var that = this;
            return (boxStateHandler) => {
                return (e) => {
                    var value = e.target.value;
                    return that.setState((state) => {
                        boxStateHandler(state.boxes[index], value);
                        return state;
                    });
                }
            }
        }

        componentDidMount() {
            this.add();
            this.add();
        }

        remove(index) {
            var that = this;
            return () => {
                return that.setState((state) => {
                    state.boxes.splice(index, 1);
                    return state;
                });
            }
        }

        add() {
            this.setState((state) => {
                state.boxes.push({
                    position: { x: 0, y: 0, z: 0 },
                    size: { x: 1, y: 1, z: 1 },
                    color: getRandomColor()
                });
                return state;
            });
        };

        render() {
            return (
                <div>
                    <div className="fixed-ratio">
                        <a-scene embedded="">
                            <a-entity position="0 0 0" camera="" look-controls=""></a-entity>
                            <a-entity position={(- this.state.boxes.length + 1) + ' 0 -5'}>
                                {this.state.boxes.map((box, index) => (
                                    <BoxAFrame instance={box} index={index} key={index} />
                                ))}
                            </a-entity>
                        </a-scene>
                    </div>
                    <form className="uk-form-stacked">
                        <ul className="uk-list">
                            {this.state.boxes.map((box, index) => (
                                <BoxControl instance={box} index={index} key={index} onChange={this.changeState(index)} remove={this.remove(index)} />
                            ))}
                        </ul>
                        <a onClick={(e) => this.add()} className="uk-link-reset"><span uk-icon="plus"></span> Přidat nový objekt</a>
                    </form>
                </div>
            );
        }
    }

    ReactDOM.render(<ExampleApp2 />, document.getElementById('app2'));

}, {
    before: '// React JSX zkompilovaný pomocí Babel.js\r\n',
    output: 'code2'
});


run(function () {

    const Seat = ({ row, column, enabled, picker }) => {
        const click = () => picker(row, column);
        return (
            <a-entity position={(column * 1.4) + ' ' + (row * -0.5) + ' ' + (row * 2.5)}>
                <a-box color={enabled ? '#0099ff' : '#ff9900'}
                    width="0.6"
                    height="0.5"
                    depth="0.5"
                    position="0 0 0"
                    data-clickable
                    onClick={click}>
                </a-box>
                <a-box color={enabled ? '#0055ee' : '#ee5500'}
                    width="0.2"
                    height="0.8"
                    depth="0.6"
                    position="0.4 0.15 0"
                    data-clickable
                    onClick={click}>
                </a-box>
                <a-box color={enabled ? '#0055ee' : '#ee5500'}
                    width="0.2"
                    height="0.8"
                    depth="0.6"
                    position="-0.4 0.15 0"
                    data-clickable
                    onClick={click}>
                </a-box>
                <a-box color={enabled ? '#0077ff' : '#ff7700'}
                    width="0.8"
                    height="1.2"
                    depth="0.1"
                    position="0 0.35 -0.25"
                    data-clickable
                    onClick={click}>
                </a-box>
            </a-entity>
        );
    }

    const SeatButton = ({ row, column, enabled, picker }) => {
        const click = () => picker(row, column);
        return (
            <input type='button' className={enabled ? 'uk-button seat-free' : 'uk-button seat-taken' } onClick={click} value={'S ' + (column + 1)} />
        );
    }

    class ExampleApp3 extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                seats: [
                    [true, true, true, true, true, true, true],
                    [true, true, true, true, true, true, true],
                    [true, true, true, true, true, true, true],
                    [true, true, true, true, true, true, true]
                ],
                free: 28,
                taken: 0
            };
        }

        getSeatPicker() {
            var that = this;
            return (row, column) => {
                that.setState((state) => {
                    state.seats[row][column] = !state.seats[row][column];
                    state.seats[row][column] ? (state.free++ , state.taken--) : (state.taken++ , state.free--);
                    return state;
                });
            }
        }
               
        render() {
            return (
                <div>
                    <div className="fixed-ratio">
                        <a-scene embedded="">
                            <a-entity position="0 0 0" camera="" look-controls=""></a-entity>
                            <a-entity light="type: directional; color: #FFF; intensity: 2; castShadow:true;" position="10 5 10"></a-entity>
                            <a-entity position={(-this.state.seats[0].length / 2) + ' 0 ' + (-this.state.seats.length * 3)}>
                                {this.state.seats.map((row, rowIndex) => (
                                    row.map((enabled, colIndex) => (
                                        <Seat row={rowIndex} column={colIndex} enabled={enabled} picker={this.getSeatPicker()} key={rowIndex + 'x' + colIndex} />
                                    ))
                                ))}
                            </a-entity>
                            <a-entity cursor="rayOrigin: mouse; fuse: false;" raycaster="objects: [data-clickable];"></a-entity>
                        </a-scene>
                    </div>
                    <form className="uk-form-stacked">
                        <h3>Vyberte sedadlo</h3>
                        <table className="uk-table uk-table-divider">
                            <caption>Obsazeno je {this.state.taken}, zbývá {this.state.free} volných míst</caption>
                            <tbody>
                                {this.state.seats.map((row, rowIndex) => (
                                    <tr key={rowIndex}>
                                        <td>Řada {rowIndex+1}</td>
                                        {row.map((enabled, colIndex) => (
                                            <td key={rowIndex + 'x' + colIndex}>
                                                <SeatButton row={rowIndex} column={colIndex} enabled={enabled} picker={this.getSeatPicker()} />
                                            </td>
                                        ))}
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </form>
                </div>
            );
        }
    }

    ReactDOM.render(<ExampleApp3 />, document.getElementById('app3'));

}, {
    before: '// React JSX zkompilovaný pomocí Babel.js\r\n',
    output: 'code3'
});