﻿run(function () {

    class ExampleApp1 extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                text: 'Hello world!',
                diffuseColor: '#dd3300',
                outlineColor: '#ee9966'
            };
            this.binder = OP({});
        }

        componentDidUpdate(prevProps, prevState, snapshot) {
            this.binder.set(this.state);
        }

        getStateUpdater() {
            var that = this;
            return (updater) => {
                return (value) => {
                    that.setState((state) => {
                        updater(value, state);
                        return state;
                    });
                }
            }
        }

        componentDidMount() {
            var scene = setupScene(this.canvas);

            var planeText = new BABYLON.Mesh.CreatePlane("planeForText", 20, scene);
            var advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(planeText, 2048, 2048);

            var text = new BABYLON.GUI.TextBlock();
            text.fontSize = 200;
            text.outlineWidth = 5;
            window.text = text;

            var state = this.getStateUpdater();
            this.binder.map(MAP.TEXT.to3DModel, text).set(this.state);
            OP(text).map({
                _text: state((v, s) => s.text = v),
                _color: state((v, s) => s.diffuseColor = v),
                _outlineColor: state((v, s) => s.outlineColor = v)
            }, {});

            advancedTexture.addControl(text);
        }

        renderScene() {
            return (
                <div className="fixed-ratio">
                    <canvas ref={ref => (this.canvas = ref)}></canvas>
                </div>
            );
        }

        renderControls() {
            return (
                <form className="uk-form-stacked">
                    <div className="uk-margin-small-top">
                        <label className="uk-form-label">Text</label>
                        <div className="uk-form-controls">
                            <input type="text" value={this.state.text} className="uk-input" onChange={(e) => this.setState({ text: e.target.value })} />
                        </div>
                    </div>
                    <div className="uk-margin-small-top">
                        <div className="uk-child-width-1-2 uk-grid-small" uk-grid="">
                            <div>
                                <label className="uk-form-label">Základní barva</label>
                                <div className="uk-form-controls">
                                    <input type="color" value={this.state.diffuseColor} onChange={(e) => this.setState({ diffuseColor: e.target.value })} />
                                    <span style={{ color: this.state.diffuseColor }}>{this.state.diffuseColor}</span>
                                </div>
                            </div>
                            <div>
                                <label className="uk-form-label">Barva okraje</label>
                                <div className="uk-form-controls">
                                    <input type="color" value={this.state.outlineColor} onChange={(e) => this.setState({ outlineColor: e.target.value })} />
                                    <span style={{ color: this.state.outlineColor }}>{this.state.outlineColor}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            );
        }

        render() {
            return (
                <div>
                    {this.renderScene()}
                    {this.renderControls()}
                </div>
            );
        }
    }

    ReactDOM.render(<ExampleApp1 />, document.getElementById('app1'));

}, {
    before: '// React JSX zkompilovaný pomocí Babel.js\r\n',
    output: 'code1'
});



(function () {

    class BoxControl extends React.Component {
        constructor(props) {
            super(props);
            this.binder = OP({});
            this.cube;
        }

        getIndexReader() {
            return () => this.props.index;
        }

        componentDidUpdate(prevProps, prevState, snapshot) {
            const { index, instance } = this.props;
            if (prevProps.index > index) {
                this.binder.set({ index });
                this.cube.position.x = this.cube.position.x - 2;
            } else {
                this.binder.set(instance);
            }
        }

        componentDidMount() {
            const { index, instance, group, scene, updater } = this.props;
            const { color } = instance;
            var state = updater(this.getIndexReader());

            var material = new BABYLON.StandardMaterial("material" + index, scene);
            var cube = new BABYLON.Mesh.CreateBox("box" + index, 1, scene);
            cube.material = material;
            group.addChild(cube);

            this.binder.map(MAP.CUBE.to3DModel, cube).set({ index }).set(instance);
            OP(cube).map({
                scaling: {
                    inner: {
                        x: state((v, s) => s.size.x = v),
                        y: state((v, s) => s.size.y = v),
                        z: state((v, s) => s.size.z = v)
                    }
                },
                position: {
                    inner: {
                        x: state((v, s) => s.position.x = (v * 1) - (this.getIndexReader()() * 2)),
                        y: state((v, s) => s.position.y = v),
                        z: state((v, s) => s.position.z = v)
                    }
                },
                material: {
                    diffuseColor: state((v, s) => s.color = v.toHexString())
                }
            }, {});

            group.position.x = - index;
            cube.position.x = (index * 2);
            this.cube = cube;
        }

        componentWillUnmount() {
            this.props.scene.removeMesh(this.cube);
        }

        render() {
            const { index, instance, updater, remove } = this.props;
            const { position, size, color } = instance;
            const onChange = (handler) => updater(this.getIndexReader())((value, box) => { handler(box, value); });
            return (
                <li>
                    <div className="uk-margin-small-top uk-box-shadow-small uk-padding-small">
                        <div className="uk-grid-small" uk-grid="">
                            <div className="uk-width-auto">
                                <label className="uk-form-label">Objekt {index + 1}</label>
                                <div className="uk-form-controls">
                                    <input type="color" value={color} uk-tooltip="Barva" onChange={onChange((box, value) => box.color = value)} />
                                    <span style={{ color: color }}> {color} </span>
                                </div>
                            </div>
                            <div className="uk-width-expand">
                                <div className="uk-child-width-1-6 uk-grid-small uk-text-center" uk-grid="">
                                    <div>
                                        <label className="uk-form-label">↔ W {Math.floor(size.x * 100)}%</label>
                                        <div className="uk-form-controls">
                                            <input type="range" value={size.x} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.x = value * 1)} />
                                        </div>
                                    </div>
                                    <div>
                                        <label className="uk-form-label">↕ H {Math.floor(size.y * 100)}%</label>
                                        <div className="uk-form-controls">
                                            <input type="range" value={size.y} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.y = value * 1)} />
                                        </div>
                                    </div>
                                    <div>
                                        <label className="uk-form-label">⤢ L {Math.floor(size.z * 100)}%</label>
                                        <div className="uk-form-controls">
                                            <input type="range" value={size.z} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.z = value * 1)} />
                                        </div>
                                    </div>
                                    <div>
                                        <label className="uk-form-label">→ X {(Math.floor(position.x * 10) / 10)}</label>
                                        <div className="uk-form-controls">
                                            <input type="range" value={position.x} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.x = value * 1)} />
                                        </div>
                                    </div>
                                    <div>
                                        <label className="uk-form-label">↑ Y {position.y}</label>
                                        <div className="uk-form-controls">
                                            <input type="range" value={position.y} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.y = value * 1)} />
                                        </div>
                                    </div>
                                    <div>
                                        <label className="uk-form-label">↗ Z {position.z}</label>
                                        <div className="uk-form-controls">
                                            <input type="range" value={position.z} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.z = value * 1)} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="uk-width-auto uk-text-right">
                                <a className="uk-icon-link" uk-tooltip="Smazat" uk-icon="icon: close; ratio: 1.5" onClick={() => remove()}></a>
                            </div>
                        </div>
                    </div>
                </li>
            );
        }
    }

    class ExampleApp2 extends React.Component {
        constructor(props) {
            super(props);
            this.state = { boxes: [] };
        }

        remove(index) {
            var that = this;
            return () => {
                that.group.position.x = - (that.state.boxes.length - 2);
                return that.setState((state) => {
                    state.boxes = state.boxes.filter((v, i) => i != index);
                    return state;
                });
            }
        }

        add() {
            this.setState((state) => {
                state.boxes.push({
                    position: { x: 0, y: 0, z: 0 },
                    size: { x: 1, y: 1, z: 1 },
                    color: getRandomColor(),
                    key: getUniqueHash()
                });
                return state;
            });
        };

        componentDidMount() {
            this.scene = setupScene(this.canvas);
            this.group = new BABYLON.AbstractMesh("group", this.scene);
            this.add();
            this.add();
        }

        getBoxStateUpdater() {
            var that = this;
            return (indexReader) => {
                return (updater) => {
                    return (value) => {
                        value = value.target && value.target.value ? value.target.value : value;
                        that.setState((state) => {
                            updater(value, state.boxes[indexReader()]);
                            return state;
                        });
                    }
                }
            }
        }

        render() {
            return (
                <div>
                    <div className="fixed-ratio">
                        <canvas ref={ref => (this.canvas = ref)}></canvas>
                    </div>
                    <form className="uk-form-stacked">
                        <ul className="uk-list">
                            {this.state.boxes.map((box, index) => (
                                <BoxControl instance={box} index={index} key={box.key} group={this.group} scene={this.scene} updater={this.getBoxStateUpdater()} remove={this.remove(index)} />
                            ))}
                        </ul>
                        <a onClick={(e) => this.add()} className="uk-link-reset"><span uk-icon="plus"></span> Přidat nový objekt</a>
                    </form>
                </div>
            );
        }
    }


    ReactDOM.render(<ExampleApp2 />, document.getElementById('app2'));

})();



(function () {

    const mapStateToProps = (state) => ({ seats: state.seats, free: state.free, taken: state.taken });

    const mapDispatchToProps = (dispatch, ownProps) => ({
        pickSeat: (row, column) => dispatch(pickSeatAction(row, column))
    });

    const SeatButton = ({ row, column, enabled, picker }) => {
        const click = () => picker(row, column);
        return (
            <input type='button' className={enabled ? 'uk-button seat-free' : 'uk-button seat-taken'} onClick={click} value={'S ' + (column + 1)} />
        );
    }

    const SeatApp = ({ seats, free, taken, pickSeat }) => {
        return (
            <form className="uk-form-stacked">
                <h3>Vyberte sedadlo</h3>
                <table className="uk-table uk-table-divider">
                    <caption>Obsazeno je {taken}, zbývá {free} volných míst</caption>
                    <tbody>
                        {seats.map((row, rowIndex) => (
                            <tr key={rowIndex}>
                                <td>Řada {rowIndex + 1}</td>
                                {row.map((enabled, colIndex) => (
                                    <td key={rowIndex + 'x' + colIndex}>
                                        <SeatButton row={rowIndex} column={colIndex} enabled={enabled} picker={pickSeat} />
                                    </td>
                                ))}
                            </tr>
                        ))}
                    </tbody>
                </table>
            </form>
        );
    }

    const ConnectedSeatApp = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(SeatApp);

    ReactDOM.render(<ReactRedux.Provider store={store}><ConnectedSeatApp /></ReactRedux.Provider>, document.getElementById('app3'));
})();


run(function (scene) {

    var rotationNode = new BABYLON.TransformNode("node");
    var positionNode = new BABYLON.TransformNode("node");

    // příprava materiálů
    var seatFreeMaterial = new BABYLON.StandardMaterial("free", scene);
    seatFreeMaterial.diffuseColor = new BABYLON.Color3(0, 0.6, 1);
    seatFreeMaterial.emissiveColor = new BABYLON.Color3(0, 0.33, 0.93);
    var seatTakenMaterial = new BABYLON.StandardMaterial("taken", scene);
    seatTakenMaterial.diffuseColor = new BABYLON.Color3(1, 0.6, 0);
    seatTakenMaterial.emissiveColor = new BABYLON.Color3(0.93, 0.33, 0);
    var backFreeMaterial = new BABYLON.StandardMaterial("backFree", scene);
    backFreeMaterial.diffuseColor = new BABYLON.Color3(0, 0.53, 1);
    backFreeMaterial.emissiveColor = new BABYLON.Color3(0, 0.33, 0.93);
    var backTakenMaterial = new BABYLON.StandardMaterial("backTaken", scene);
    backTakenMaterial.diffuseColor = new BABYLON.Color3(1, 0.53, 0);
    backTakenMaterial.emissiveColor = new BABYLON.Color3(0.93, 0.33, 0);
    var holderFreeMaterial = new BABYLON.StandardMaterial("holderFree", scene);
    holderFreeMaterial.diffuseColor = new BABYLON.Color3(0, 0.33, 0.93);
    holderFreeMaterial.emissiveColor = new BABYLON.Color3(0, 0.33, 0.93);
    var holderTakenMaterial = new BABYLON.StandardMaterial("holderTaken", scene);
    holderTakenMaterial.diffuseColor = new BABYLON.Color3(0.93, 0.33, 0);
    holderTakenMaterial.emissiveColor = new BABYLON.Color3(0.93, 0.33, 0);

    // Sedadlo
    function Seat(row, column) {
        var seat = new BABYLON.TransformNode("seat" + row + "x" + column);
        seat.position = new BABYLON.Vector3((column * 1.4), (row * -0.5), (row * -3));
        var bottom = BABYLON.MeshBuilder.CreateBox("bottom" + row + "x" + column, { width: 0.6, height: 0.5, depth: 0.5 }, scene);
        var right = BABYLON.MeshBuilder.CreateBox("right" + row + "x" + column, { width: 0.2, height: 0.8, depth: 0.6 }, scene);
        right.position.x = 0.4;
        right.position.y = 0.15;
        var left = BABYLON.MeshBuilder.CreateBox("left" + row + "x" + column, { width: 0.2, height: 0.8, depth: 0.6 }, scene);
        left.position.x = -0.4;
        left.position.y = 0.15;
        var back = BABYLON.MeshBuilder.CreateBox("back" + row + "x" + column, { width: 0.8, height: 1.2, depth: 0.1 }, scene);
        back.position.y = 0.35;
        back.position.z = 0.25;

        // předání akce pro výběr sedadla do Redux úložiště
        function click() { store.dispatch(pickSeatAction(row, column)); }

        // připojení listeneru na kliknutí
        bottom.actionManager = new BABYLON.ActionManager(scene);
        bottom.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger, click));
        right.actionManager = new BABYLON.ActionManager(scene);
        right.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger, click));
        left.actionManager = new BABYLON.ActionManager(scene);
        left.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger, click));
        back.actionManager = new BABYLON.ActionManager(scene);
        back.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger, click));

        // metoda pro přepnutí
        this.switch = function (state) {
            if (state) {
                bottom.material = seatFreeMaterial;
                right.material = holderFreeMaterial;
                left.material = holderFreeMaterial;
                back.material = backFreeMaterial;
            } else {
                bottom.material = seatTakenMaterial;
                right.material = holderTakenMaterial;
                left.material = holderTakenMaterial;
                back.material = backTakenMaterial;
            }
            bottom.markAsDirty();
            right.markAsDirty();
            left.markAsDirty();
            back.markAsDirty();
        }

        bottom.parent = seat;
        right.parent = seat;
        left.parent = seat;
        back.parent = seat;
        seat.parent = positionNode;
    }

    // vytvoření sedadel na scéně
    var state = store.getState();
    var seats = [];
    for (var r = 0; r < state.seats.length; r++) {
        seats.push([]);
        for (var c = 0; c < state.seats[r].length; c++) {
            var seat = new Seat(r, c);
            seat.switch(state.seats[r][c]);
            seats[r].push(seat);
        }
    }
    positionNode.position = new BABYLON.Vector3((-state.seats[0].length / 2) - 0.5, 0, (state.seats.length * 1.8));
    positionNode.parent = rotationNode;
    rotationNode.rotation.x = - (Math.PI / 6);

    // připojení na Redux úložiště
    store.subscribe(function () {
        state = store.getState();
        for (var r = 0; r < state.seats.length; r++) {
            for (var c = 0; c < state.seats[r].length; c++) {
                seats[r][c].switch(state.seats[r][c]);
            }
        }
    });

}, {
    canvas: 'canvas3',
    output: 'code3'
});