﻿run(function () {

    class ExampleApp1 extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                text: 'Hello world!',
                diffuseColor: '#dd3300',
                outlineColor: '#ee9966'
            };
            this.binder = OP({});
        }

        componentDidUpdate(prevProps, prevState, snapshot) {
            this.binder.set(this.state);
        }

        getStateUpdater() {
            var that = this;
            return (updater) => {
                return (value) => {
                    that.setState((state) => {
                        updater(value, state);
                        return state;
                    });
                }
            }
        }

        componentDidMount() {
            var scene = setupScene(this.canvas);

            var font = new pc.Asset('Arial.json', "font", { url: "../../../libs/playcanvas/Arial.json" });

            var that = this;
            scene.assets.on('load', function () {

                var text = new pc.Entity("text", scene);
                text.addComponent("element", {
                    type: "text",
                    anchor: [0, 0, 0, 0],
                    pivot: [0.5, 0.5],
                    fontSize: 16,
                    fontAsset: font,
                    opacity: 1,
                    color: [0, 0, 1],
                    shadowColor: [0, 0, 1],
                    shadowOffset: [0.2, -0.2],
                    text: that.state.text
                });
                text.setLocalScale(1 / 32, 1 / 32, 1 / 32);
                scene.root.addChild(text);
                window.text = text;

                var state = that.getStateUpdater();
                that.binder.map(MAP.TEXT.to3DModel, text).set(that.state);
                OP(text).map({
                    text: state((v, s) => s.text = v),
                    color: state((v, s) => s.diffuseColor = v),
                    shadowColor: state((v, s) => s.outlineColor = v)
                }, {});
            });

            scene.assets.add(font);
            scene.assets.load(font);
        }

        renderScene() {
            return (
                <div className="fixed-ratio">
                    <canvas ref={ref => (this.canvas = ref)}></canvas>
                </div>
            );
        }

        renderControls() {
            return (
                <form className="uk-form-stacked">
                    <div className="uk-margin-small-top">
                        <label className="uk-form-label">Text</label>
                        <div className="uk-form-controls">
                            <input type="text" value={this.state.text} className="uk-input" onChange={(e) => this.setState({ text: e.target.value })} />
                        </div>
                    </div>
                    <div className="uk-margin-small-top">
                        <div className="uk-child-width-1-2 uk-grid-small" uk-grid="">
                            <div>
                                <label className="uk-form-label">Základní barva</label>
                                <div className="uk-form-controls">
                                    <input type="color" value={this.state.diffuseColor} onChange={(e) => this.setState({ diffuseColor: e.target.value })} />
                                    <span style={{ color: this.state.diffuseColor }}>{this.state.diffuseColor}</span>
                                </div>
                            </div>
                            <div>
                                <label className="uk-form-label">Barva okraje</label>
                                <div className="uk-form-controls">
                                    <input type="color" value={this.state.outlineColor} onChange={(e) => this.setState({ outlineColor: e.target.value })} />
                                    <span style={{ color: this.state.outlineColor }}>{this.state.outlineColor}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            );
        }

        render() {
            return (
                <div>
                    {this.renderScene()}
                    {this.renderControls()}
                </div>
            );
        }
    }

    ReactDOM.render(<ExampleApp1 />, document.getElementById('app1'));

}, {
    before: '// React JSX zkompilovaný pomocí Babel.js\r\n',
    output: 'code1'
});



(function () {

    class BoxControl extends React.Component {
        constructor(props) {
            super(props);
            this.binder = OP({});
            this.cube;
        }

        getIndexReader() {
            return () => this.props.index;
        }

        componentDidUpdate(prevProps, prevState, snapshot) {
            const { index, instance } = this.props;
            if (prevProps.index > index) {
                this.binder.set({ index });
                var original = this.cube.getLocalPosition();
                original.x = original.x - 2;
                this.cube.setLocalPosition(original);
            } else {
                this.binder.set(instance);
            }
        }

        componentDidMount() {
            const { index, instance, group, scene, updater } = this.props;
            const { color } = instance;
            var state = updater(this.getIndexReader());

            var cube = new pc.Entity("box" + index, scene);
            cube.addComponent("model", { type: "box" });

            var material = new pc.StandardMaterial();
            material.update();
            cube.model.material = material;

            group.addChild(cube);

            this.binder.map(MAP.CUBE.to3DModel, cube).set({ index }).set(instance);

            group.setLocalPosition(-index * 0.2, 0, 0);
            cube.setLocalPosition(index * 2, 0, 0);
            this.cube = cube;
        }

        componentWillUnmount() {
            this.props.group.removeChild(this.cube);
            this.cube.destroy();
        }

        render() {
            const { index, instance, updater, remove } = this.props;
            const { position, size, color } = instance;
            const onChange = (handler) => updater(this.getIndexReader())((value, box) => { handler(box, value); });
            return (
                <li>
                    <div className="uk-margin-small-top uk-box-shadow-small uk-padding-small">
                        <div className="uk-grid-small" uk-grid="">
                            <div className="uk-width-auto">
                                <label className="uk-form-label">Objekt {index + 1}</label>
                                <div className="uk-form-controls">
                                    <input type="color" value={color} uk-tooltip="Barva" onChange={onChange((box, value) => box.color = value)} />
                                    <span style={{ color: color }}> {color} </span>
                                </div>
                            </div>
                            <div className="uk-width-expand">
                                <div className="uk-child-width-1-6 uk-grid-small uk-text-center" uk-grid="">
                                    <div>
                                        <label className="uk-form-label">↔ W {Math.floor(size.x * 100)}%</label>
                                        <div className="uk-form-controls">
                                            <input type="range" value={size.x} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.x = value * 1)} />
                                        </div>
                                    </div>
                                    <div>
                                        <label className="uk-form-label">↕ H {Math.floor(size.y * 100)}%</label>
                                        <div className="uk-form-controls">
                                            <input type="range" value={size.y} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.y = value * 1)} />
                                        </div>
                                    </div>
                                    <div>
                                        <label className="uk-form-label">⤢ L {Math.floor(size.z * 100)}%</label>
                                        <div className="uk-form-controls">
                                            <input type="range" value={size.z} className="uk-range" min={0.1} max={2} step={0.1} onChange={onChange((box, value) => box.size.z = value * 1)} />
                                        </div>
                                    </div>
                                    <div>
                                        <label className="uk-form-label">→ X {(Math.floor(position.x * 10) / 10)}</label>
                                        <div className="uk-form-controls">
                                            <input type="range" value={position.x} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.x = value * 1)} />
                                        </div>
                                    </div>
                                    <div>
                                        <label className="uk-form-label">↑ Y {position.y}</label>
                                        <div className="uk-form-controls">
                                            <input type="range" value={position.y} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.y = value * 1)} />
                                        </div>
                                    </div>
                                    <div>
                                        <label className="uk-form-label">↗ Z {position.z}</label>
                                        <div className="uk-form-controls">
                                            <input type="range" value={position.z} className="uk-range" min={-1} max={1} step={0.1} onChange={onChange((box, value) => box.position.z = value * 1)} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="uk-width-auto uk-text-right">
                                <a className="uk-icon-link" uk-tooltip="Smazat" uk-icon="icon: close; ratio: 1.5" onClick={() => remove()}></a>
                            </div>
                        </div>
                    </div>
                </li>
            );
        }
    }

    class ExampleApp2 extends React.Component {
        constructor(props) {
            super(props);
            this.state = { boxes: [] };
        }

        remove(index) {
            var that = this;
            return () => {
                that.group.setLocalPosition((-(that.state.boxes.length - 2) * 0.2), 0, 0);
                return that.setState((state) => {
                    state.boxes = state.boxes.filter((v, i) => i != index);
                    return state;
                });
            }
        }

        add() {
            this.setState((state) => {
                state.boxes.push({
                    position: { x: 0, y: 0, z: 0 },
                    size: { x: 1, y: 1, z: 1 },
                    color: getRandomColor(),
                    key: getUniqueHash()
                });
                return state;
            });
        };

        componentDidMount() {
            this.scene = setupScene(this.canvas);
            this.group = new pc.Entity("group", this.scene);
            this.group.setLocalScale(0.2, 0.2, 0.2);
            this.scene.root.addChild(this.group);
            this.add();
            this.add();
        }

        getBoxStateUpdater() {
            var that = this;
            return (indexReader) => {
                return (updater) => {
                    return (value) => {
                        value = value.target && value.target.value ? value.target.value : value;
                        that.setState((state) => {
                            updater(value, state.boxes[indexReader()]);
                            return state;
                        });
                    }
                }
            }
        }

        render() {
            return (
                <div>
                    <div className="fixed-ratio">
                        <canvas ref={ref => (this.canvas = ref)}></canvas>
                    </div>
                    <form className="uk-form-stacked">
                        <ul className="uk-list">
                            {this.state.boxes.map((box, index) => (
                                <BoxControl instance={box} index={index} key={box.key} group={this.group} scene={this.scene} updater={this.getBoxStateUpdater()} remove={this.remove(index)} />
                            ))}
                        </ul>
                        <a onClick={(e) => this.add()} className="uk-link-reset"><span uk-icon="plus"></span> Přidat nový objekt</a>
                    </form>
                </div>
            );
        }
    }


    ReactDOM.render(<ExampleApp2 />, document.getElementById('app2'));

})();



(function () {

    const mapStateToProps = (state) => ({ seats: state.seats, free: state.free, taken: state.taken });

    const mapDispatchToProps = (dispatch, ownProps) => ({
        pickSeat: (row, column) => dispatch(pickSeatAction(row, column))
    });

    const SeatButton = ({ row, column, enabled, picker }) => {
        const click = () => picker(row, column);
        return (
            <input type='button' className={enabled ? 'uk-button seat-free' : 'uk-button seat-taken'} onClick={click} value={'S ' + (column + 1)} />
        );
    }

    const SeatApp = ({ seats, free, taken, pickSeat }) => {
        return (
            <form className="uk-form-stacked">
                <h3>Vyberte sedadlo</h3>
                <table className="uk-table uk-table-divider">
                    <caption>Obsazeno je {taken}, zbývá {free} volných míst</caption>
                    <tbody>
                        {seats.map((row, rowIndex) => (
                            <tr key={rowIndex}>
                                <td>Řada {rowIndex + 1}</td>
                                {row.map((enabled, colIndex) => (
                                    <td key={rowIndex + 'x' + colIndex}>
                                        <SeatButton row={rowIndex} column={colIndex} enabled={enabled} picker={pickSeat} />
                                    </td>
                                ))}
                            </tr>
                        ))}
                    </tbody>
                </table>
            </form>
        );
    }

    const ConnectedSeatApp = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(SeatApp);

    ReactDOM.render(<ReactRedux.Provider store={store}><ConnectedSeatApp /></ReactRedux.Provider>, document.getElementById('app3'));
})();


run(function (scene) {

    var rotationNode = new pc.Entity("rotated", scene);
    var positionNode = new pc.Entity("positioned", scene);

    // příprava materiálů
    var seatFreeMaterial = new pc.StandardMaterial({});
    seatFreeMaterial.diffuse = new pc.Color(0, 0.6, 1);
    seatFreeMaterial.emissive = new pc.Color(0, 0.33, 0.9);
    seatFreeMaterial.update();
    var seatTakenMaterial = new pc.StandardMaterial({});
    seatTakenMaterial.diffuse = new pc.Color(1, 0.6, 0);
    seatTakenMaterial.emissive = new pc.Color(0.9, 0.33, 0);
    seatTakenMaterial.update();
    var backFreeMaterial = new pc.StandardMaterial({});
    backFreeMaterial.diffuse = new pc.Color(0, 0.53, 1);
    backFreeMaterial.emissive = new pc.Color(0, 0.33, 0.9);
    backFreeMaterial.update();
    var backTakenMaterial = new pc.StandardMaterial({});
    backTakenMaterial.diffuse = new pc.Color(1, 0.53, 0);
    backTakenMaterial.emissive = new pc.Color(0.9, 0.33, 0);
    backTakenMaterial.update();
    var holderFreeMaterial = new pc.StandardMaterial({});
    holderFreeMaterial.diffuse = new pc.Color(0, 0.33, 0.9);
    holderFreeMaterial.emissive = new pc.Color(0, 0.33, 0.9);
    holderFreeMaterial.update();
    var holderTakenMaterial = new pc.StandardMaterial({});
    holderTakenMaterial.diffuse = new pc.Color(0.9, 0.33, 0);
    holderTakenMaterial.emissive = new pc.Color(0.9, 0.33, 0);
    holderTakenMaterial.update();

    // Sedadlo
    function Seat(row, column) {
        var seat = new pc.Entity("seat" + row + "x" + column, scene);
        seat.setLocalPosition((column * 1.4), (row * -0.5), (row * 3));
        var bottom = new pc.Entity("bottom" + row + "x" + column, scene);
        bottom.addComponent("model", { type: "box" });
        bottom.setLocalScale(0.6, 0.5, 0.6);
        var right = new pc.Entity("right" + row + "x" + column, scene);
        right.addComponent("model", { type: "box" });
        right.setLocalScale(0.2, 0.8, 0.6);
        right.setLocalPosition(0.4, 0.15, 0);
        var left = new pc.Entity("left" + row + "x" + column, scene);
        left.addComponent("model", { type: "box" });
        left.setLocalScale(0.2, 0.8, 0.6);
        left.setLocalPosition(-0.4, 0.15, 0);
        var back = new pc.Entity("back" + row + "x" + column, scene);
        back.addComponent("model", { type: "box" });
        back.setLocalScale(0.8, 1.2, 0.1);
        back.setLocalPosition(0, 0.35, -0.25);

        // předání akce pro výběr sedadla do Redux úložiště
        function click() { store.dispatch(pickSeatAction(row, column)); }

        // připojení listeneru na kliknutí
        bottom.onclick = click;
        right.onclick = click;
        left.onclick = click;
        back.onclick = click;

        // metoda pro přepnutí
        this.switch = function (state) {
            if (state) {
                bottom.model.material = seatFreeMaterial;
                right.model.material = holderFreeMaterial;
                left.model.material = holderFreeMaterial;
                back.model.material = backFreeMaterial;
            } else {
                bottom.model.material = seatTakenMaterial;
                right.model.material = holderTakenMaterial;
                left.model.material = holderTakenMaterial;
                back.model.material = backTakenMaterial;
            }
        }

        seat.addChild(bottom);
        seat.addChild(right);
        seat.addChild(left);
        seat.addChild(back);
        positionNode.addChild(seat);
    }

    // vytvoření sedadel na scéně
    var state = store.getState();
    var seats = [];
    for (var r = 0; r < state.seats.length; r++) {
        seats.push([]);
        for (var c = 0; c < state.seats[r].length; c++) {
            var seat = new Seat(r, c);
            seat.switch(state.seats[r][c]);
            seats[r].push(seat);
        }
    }
    positionNode.setLocalPosition((-state.seats[0].length / 2) - 0.5, 0, (-state.seats.length * 1.5));
    rotationNode.addChild(positionNode);
    rotationNode.setLocalScale(0.2, 0.2, 0.2);
    rotationNode.rotateLocal(30, 0, 0);
    scene.root.addChild(rotationNode);

    // připojení na Redux úložiště
    store.subscribe(function () {
        state = store.getState();
        for (var r = 0; r < state.seats.length; r++) {
            for (var c = 0; c < state.seats[r].length; c++) {
                seats[r][c].switch(state.seats[r][c]);
            }
        }
    });

}, {
    canvas: 'canvas3',
    output: 'code3'
});