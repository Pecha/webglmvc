﻿function setupScene(element) {
    var canvas = typeof (element) === "string" ? document.getElementById(element) : element;

    var app = new pc.Application(canvas, {
        mouse: new pc.Mouse(canvas),
        touch: new pc.TouchDevice(canvas),
        keyboard: new pc.Keyboard(window),
        graphicsDeviceOptions: {
            antialias: true,
            alpha: false,
            preserveDrawingBuffer: false
        },
        assetPrefix: window.ASSET_PREFIX || '',
        scriptPrefix: window.SCRIPT_PREFIX || '',
        scriptsOrder: window.SCRIPTS || []
    });
    app.start();

    scriptLoader(app)("OrbitCamera", "TouchInput", "MouseInput", "PickerFramebuffer");

    app.setCanvasFillMode(pc.FILLMODE_NONE);
    app.setCanvasResolution(pc.RESOLUTION_AUTO);

    window.addEventListener('resize', function () {
        app.resizeCanvas();
    });

    var camera = new pc.Entity('camera', app);
    camera.addComponent('script', {
        enabled: true,
        order: [
            "orbitCamera",
            "touchInput",
            "mouseInput",
            "pickerFramebuffer"
        ],
        scripts: {
            orbitCamera: {
                enabled: true,
                attributes: {
                    distanceMin: 0,
                    distanceMax: 0,
                    pitchAngleMin: -90,
                    pitchAngleMax: 90,
                    frameOnStart: true,
                    inertiaFactor: 0
                }
            },
            touchInput: {
                enabled: true,
                attributes: {
                    orbitSensitivity: 0.4,
                    distanceSensitivity: 0.2
                }
            },
            mouseInput: {
                enabled: true,
                attributes: {
                    orbitSensitivity: 0.3,
                    distanceSensitivity: 0.18
                }
            },
            pickerFramebuffer: {
                enabled: true
            }
        }
    });
    camera.addComponent('camera', {
        enabled: true,
        clearColor: [0.13, 0.13, 0.13, 0]
    });
    app.root.addChild(camera);
    camera.setPosition(0, 0, 3);
    app.camera = camera;

    var light = new pc.Entity('light', app);
    light.addComponent('light');
    app.root.addChild(light);
    light.setEulerAngles(45, 0, 0);

    return app;
}

var MAP = {

    TEXT: {
        to3DModel: {
            text: (v, m) => m().element.text = v,
            diffuseColor: (v, m) => m().element.color = (new pc.Color()).fromString(v),
            outlineColor: (v, m) => m().element.shadowColor = (new pc.Color()).fromString(v)
        },
        toDataModel: {
            element: {
                text: (v, m) => m().text = v,
                color: (v, m) => m().diffuseColor = v.toString(),
                shadowColor: (v, m) => m().outlineColor = v.toString()
            }
        }
    },

    CUBE: {
        to3DModel: {
            size: {
                x: {
                    get: m => m().getLocalScale().x,
                    set: (v, m) => { var o = m().getLocalScale(); o.x = v; m().setLocalScale(o); }
                },
                y: {
                    get: m => m().getLocalScale().y,
                    set: (v, m) => { var o = m().getLocalScale(); o.y = v; m().setLocalScale(o); }
                },
                z: {
                    get: m => m().getLocalScale().z,
                    set: (v, m) => { var o = m().getLocalScale(); o.z = v; m().setLocalScale(o); }
                },
            },
            position: {
                x: {
                    get: m => (m().getLocalPosition().x * 1) - (m.root.ref.index * 2),
                    set: (v, m) => { var o = m().getLocalPosition(); o.x = (m.root.ref.index * 2) + (v * 1); m().setLocalPosition(o); }
                },
                y: {
                    get: m => m().getLocalPosition().y,
                    set: (v, m) => { var o = m().getLocalPosition(); o.y = v; m().setLocalPosition(o); }
                },
                z: {
                    get: m => m().getLocalPosition().z,
                    set: (v, m) => { var o = m().getLocalPosition(); o.z = v; m().setLocalPosition(o); }
                },
            },
            color: (v, m) => { m().model.material.diffuse = (new pc.Color()).fromString(v); m().model.material.update(); }
        },
        toDataModel: {
            /* todo */
        }
    }

};