﻿function setupScene(element) {
    var canvas = typeof (element) === "string" ? document.getElementById(element) : element;

    var engine = new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true });

    var scene = new BABYLON.Scene(engine);
    scene.clearColor = new BABYLON.Color4(0, 0, 0, 0);

    var light = new BABYLON.HemisphericLight("hemiLight", new BABYLON.Vector3(10, 5, -10), scene);
    light.diffuse = new BABYLON.Color3(1, 1, 1);

    var camera = new BABYLON.ArcRotateCamera("camera", 0, 0, 0, new BABYLON.Vector3(0, 0, -10), scene);
    camera.speed = 0.5;
    camera.keysDown = [40, 83];
    camera.keysUp = [38, 87];
    camera.keysLeft = [37, 65];
    camera.keysRight = [39, 68];
    camera.setTarget(BABYLON.Vector3.Zero());
    camera.attachControl(canvas, true);

    engine.runRenderLoop(function () {
        scene.render();
    });

    return scene;
}

var MAP = {

    TEXT: {
        to3DModel: {
            text: (v, m) => m().text = v,
            diffuseColor: (v, m) => m().color = v,
            outlineColor: (v, m) => m().outlineColor = v
        },
        toDataModel: {
            _text: (v, m) => m().text = v,
            _color: (v, m) => m().diffuseColor = v,
            _outlineColor: (v, m) => m().outlineColor = v
        }
    },

    CUBE: {
        to3DModel: {
            size: {
                to: m => m.scaling,
                inner: {
                    x: (v, m) => m().x = v,
                    y: (v, m) => m().y = v,
                    z: (v, m) => m().z = v
                }
            },
            position: {
                to: m => m.position,
                inner: {
                    x: {
                        get: m => (m().x * 1) - (m.root.ref.index * 2),
                        set: (v, m) => m().x = (m.root.ref.index * 2) + (v * 1)
                    },
                    y: (v, m) => m().y = v,
                    z: (v, m) => m().z = v
                }
            },
            color: (v, m) => m().material.diffuseColor.copyFrom(BABYLON.Color3.FromHexString(v))
        },
        toDataModel: {
            scaling: {
                to: m => m.size,
                inner: {
                    x: (v, m) => m().x = v,
                    y: (v, m) => m().y = v,
                    z: (v, m) => m().z = v
                }
            },
            position: {
                to: m => m.position,
                inner: {
                    x: (v, m) => m().x = (v * 1) - (m.root.target.index * 2),
                    y: (v, m) => m().y = v,
                    z: (v, m) => m().z = v
                }
            },
            material: {
                diffuseColor: (v, m) => m().color = v.toHexString()
            }
        }
    }

};