﻿function pickSeatAction(row, column) { return { type: 'PICK_SEAT', row, column }; }

function SeatReducer(state, action) {
    if (typeof state === 'undefined') {
        return {
            seats: [
                [true, true, true, true, true, true, true],
                [true, true, true, true, true, true, true],
                [true, true, true, true, true, true, true],
                [true, true, true, true, true, true, true]
            ],
            free: 28,
            taken: 0
        }
    }
    switch (action.type) {
        case 'PICK_SEAT':
            const newState = Object.assign({}, state);
            newState.seats[action.row][action.column] = !state.seats[action.row][action.column];
            newState.seats[action.row][action.column] ? (newState.free++ , newState.taken--) : (newState.taken++ , newState.free--);
            return newState;
        default:
            return state;
    }
}

var store = Redux.createStore(SeatReducer);