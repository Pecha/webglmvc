﻿function ready(fn, document) { document = (document !== undefined) ? document : ((window !== undefined) ? window.document : undefined); (document !== undefined) ? (document.readyState !== 'complete' ? document.addEventListener('readystatechange', function () { document.readyState === 'complete' ? fn() : 0; }) : fn()) : fn(); }

function run(fn, options) {
    var before = options.before || '// Předpoklad: Vytvořená základní scéna s kamerou a světelným zdrojem \r\n';
     var stringified = before + fn.toString();
    ready(function () {
        if (options.output) {
            var codeBlock = document.getElementById(options.output);
            codeBlock.innerHTML = stringified;
            hljs.highlightBlock(codeBlock);
        }
        if (typeof (setupScene) !== "undefined" && typeof (options.canvas) !== "undefined") {
            var scene = setupScene(options.canvas);
            fn.apply(scene, [scene]);
        } else {
            fn();
        }
    });
}

function toBin(v) {
    var x = (v * 1).toString(2);
    return ("00000000").substring(0, 8 - x.length) + x;
}

function toHex(v) {
    var x = (v * 1).toString(16);
    return ("00").substring(0, 2 - x.length) + x;
}

function getRandomNumber() {
    var lim;
    var args = arguments;
    args.length == 2 ? lim = { min: args[0], max: args[1] } : args.length == 1 ? lim = { min: 0, max: args[0] } : lim = { min: 0, max: 10000000000000000 };
    return Math.floor((Math.random() * ((lim.max - lim.min) + 1)) + lim.min);
}

function getRandomHexadecimal() {
    return toHex(getRandomNumber(0, 255));
}

function getRandomColor() {
    return '#' + getRandomHexadecimal() + getRandomHexadecimal() + getRandomHexadecimal();
}

function getRandomHash() {
    return getRandomNumber().toString(36);
}

var getUniqueHash = (function () {
    var taken = [];
    var isRunning = false; 
    return function () {
        while (isRunning) { }
        isRunning = true;
        var hash = getRandomHash() + getRandomHash();
        while (taken.indexOf(hash) > -1) { hash = getRandomHash() + getRandomHash(); }
        taken.push(hash);
        isRunning = false;
        return hash;
    }
})();

function getDebounced(fn, time) {
    var id;
    return function () {
        var args = arguments;
        if (id) { clearTimeout(id); }
        id = setTimeout(function () { id = null; fn.apply({}, args); }, time);
    }
}


// HLJS
if(typeof (H_js) !== "undefined")
{
    hljs.registerLanguage('jsx', H_js);
}
hljs.initHighlightingOnLoad();

// Loading
document.documentElement.className = "busy";
ready(function () { setTimeout(function () { document.documentElement.className = ""; }, 1000); });
window.addEventListener("beforeunload", function () {
    if (document.documentElement.className = "busy") { }
});