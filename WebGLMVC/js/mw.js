﻿/**
 * Pomocné ObjectProxy nad odkazem na objekt.
 * @author Jan Pecha
 */
var OP = (function () {

    function async(fn) { setTimeout(fn, 0); }

    /**
     * Vytvoří ObjectProxy nad odkazem na objekt.
     * @param {any} ref
     */
    function ObjectProxy(ref) {
        ref = ref != undefined ? ref : {};

        /**
         * Vrací true pokud je objekt instancí jedné z tříd předaných v parametrech.
         * @param {...function} classConstructor
         */
        function is(classConstructor) {
            var i = 0, rv = false;
            while (i <= arguments.length && rv == false) { ref.constructor == arguments[i] ? rv = true : i++; }
            return rv;
        }

        /**
         * Aplikuje na předanou funkci odkazovaný objekt jako this + předá v parametrech.
         * @param {function} fn
         */
        function apply(fn) {
            fn.apply(ref, [ref, op]);
            return op;
        }

        /**
         * Vrací hodnotu podle klíče nebo výrazu.
         * @param {string|function} [key]
         * @returns {any}
         */
        function get(key) {
            function inner(key) {
                if (key == undefined) { return function (v) { return v; }; }
                if (key.constructor == Function) { return key; }
                if (key.constructor == String || key.constructor == Number) { return function (v) { return v[key]; }; }
            }
            return inner(key)(ref);
        }

        /**
         * Nastaví hodnotu podle klíče.
         * @param {string} key
         * @param {any} value
         */
        /**
         * Nastaví hodnoty podle objektu hodnot = multiple set.
         * @param {object} data
         */
        function set(key, value) {
            var args = arguments;
            function inner(value, key) { ref[key] = value; };
            args.length == 2 ? inner(args[1], args[0]) : OP(args[0]).each(inner);
            return op;
        }

        /**
         * Definuje vlastnost objektu podle klíče a descriptoru.
         * @param {string} key
         * @param {object} descriptor
         */
        /**
         * Definuje vlastnosti podle modelu = multiple descriptor.
         * @param {object} model
         */
        function define(key, descriptor) {
            var args = arguments;
            function inner(descriptor, key) { Object.defineProperty(ref, key, descriptor); };
            args.length == 2 ? inner(args[1], args[0]) : OP(args[0]).each(inner);
            return op;
        }

        /**
         * Definuje vlastnost objektu podle klíče a descriptoru; funguje i na configurable = false.
         * @param {string} key
         * @param {object} descriptor
         */
        /**
         * Definuje vlastnosti podle modelu = multiple descriptor; funguje i na configurable = false.
         * @param {object} model
         */
        function defineForce(key, descriptor) {
            var args = arguments;
            function inner(descriptor, key) { var value = ref[key]; delete ref[key]; define(key, descriptor); ref[key] = value; }
            args.length == 2 ? inner(args[1], args[0]) : OP(args[0]).each(inner);
            return op;
        }

        /**
         * Definuje konstantní vlastnost objektu podle klíče a hodnoty.
         * @param {string} key
         * @param {any} constValue
         * @returns OP
         */
        /**
         * Definuje konstantní vlastnosti podle objektu hodnot.
         * @param {object} constData
         */
        function defineFinal(key, constValue) {
            var args = arguments;
            function inner(value, key) { define(key, { value: value }); }
            args.length == 2 ? inner(args[1], args[0]) : OP(args[0]).each(inner);
            return op;
        }

        /**
         * Vrací descriptor podle klíče, respektuje dědičnost tříd.
         * @param {string} key
         * @returns {object}
         */
        function getDescriptor(key) {
            var level = 0, maxLevel = 100, upperType;
            function inner(type) {
                level++;
                if (type !== undefined && upperType != type && level < maxLevel) {
                    upperType = type;
                    var descriptor = Object.getOwnPropertyDescriptor(type, key);
                    return descriptor || inner(type.constructor.prototype);
                }
            }
            return inner(ref);
        }

        /**
         * Vrací objekt obsahující hodnotu a descriptor.
         * @param {string} key
         * @returns {object}
         */
        function getValueModel(key) {
            var value = ref[key];
            var descriptor = getDescriptor(key);
            return descriptor ? {
                value: value,
                descriptor: descriptor,
                get: descriptor.get,
                set: descriptor.set,
                enumerable: descriptor.enumerable,
                configurable: descriptor.configurable
            } : { value: value };
        }

        /**
         * @callback EachCallback
         * @param {any} [value] - Položka nebo vlastnost
         * @param {number|string} [identifier] - Index nebo klíč
         * @param {any} [target] - Pole nebo objekt
         */
        /**
         * Provede operaci pro každý prvek v daném objektu nebo poli.
         * @param {EachCallback} fn
         */
        function each(fn) {
            if (ref.each) { return function () { ref.each(fn); return op; } }
            if (ref.length) { return (function (fn) { for (var i = 0; i < ref.length; i++) { try { fn.apply(ref, [ref[i], i, ref]); } catch (e) { } } return op; })(fn); }
            if (ref.hasNext && ref.getNext) { return (function (fn) { while (ref.hasNext()) { fn.apply(ref, [ref.getNext(), ref]); } return op; })(fn); }
            return (function (fn) { for (var i in ref) { try { fn.apply(ref, [ref[i], i, ref]); } catch (e) { } } return op; })(fn);
        }

        /**
         * @callback MapSetterCallback
         * @param {any} [value]
         * @param {function|object} [bridge] - Přemostění na cíl
         */
        /**
         * @callback MapGetterCallback
         * @param {function|object} [bridge] - Přemostění na cíl
         * @returns {any}
         */
        /**
         * @callback MapToCallback
         * @param {object} target - Původní cíl
         * @returns {object} - Nový cíl
         */
        /**
         * Nastaví mapu na cíl.
         * @param {object} map
         * @param {MapSetterCallback} [map.set] - Setter s přemostěním na cíl
         * @param {MapGetterCallback} [map.get] - Getter s přemostěním na cíl
         * @param {MapToCallback} [map.to] - Zkratka pro přemostění
         * @param {object} [map.inner] - Vnořený objekt 
         * @param {object} target - Cíl
         */
        function map(map, target) {

            // Uložení kořenů mapy
            var root = { ref: ref, target: target };

            /**
             * Nasazení mapy (všechny parametry v dané úrovni)
             * @param {object} ref
             * @param {object} map
             * @param {object} target
             */
            function applyMap(ref, map, target) {

                OP(map).each(function (type, key) {
                    
                    // Setter auto-mapping
                    if (type && type === true) { type = function (v) { target[key] = v; }; }
                    // Setter callback
                    if (type && type.constructor == Function) { type = { set: type }; }

                    // Mapování objektu
                    if (type && type.constructor == Object) {

                        var private;

                        // Zjištění obsažených funkcí mapy
                        var hasTo = type.to && type.to.constructor == Function;
                        var hasGet = type.get && type.get.constructor == Function;
                        var hasSet = type.set && type.set.constructor == Function;
                        var hasInner = type.inner && type.inner.constructor == Object;

                        // Zkratka pro mapování
                        function to() { return hasTo ? type.to.apply(target, [target]) : target; }

                        if ((hasTo && !hasInner) || hasGet || hasSet) {       

                            // Získání původní
                            var refOP = OP(ref);
                            var old = refOP.getValueModel(key);
                            if (!old.configurable) { delete ref[key]; }

                            // Funkce pro přemostění na cíl
                            function bridge() {
                                var args = arguments;
                                switch (args.length) {
                                    // 2 argumenty -> setter, přemostění
                                    case 2: to()[args[0]] = args[1]; return to();
                                    // 1 argument -> getter
                                    case 1: return to()[args[0]];
                                    // žádný -> přemostění
                                    default: return to();
                                }
                            }
                            // Odkazy na kořeny mapy, zdroj, cíl a zkratku
                            OP(bridge).set({
                                root: root,
                                ref: ref,
                                target: target,
                                to: to()
                            });

                            /** 
                             * Getter
                             */
                            function get() {
                                // Get pomocí mapy
                                if (hasGet) { return type.get.apply(bridge, [bridge]); }
                                // Starý get
                                if (old.get) { return old.get.apply(ref, []); }
                                // Žádný get -> privátní hodnota
                                return private;
                            }

                            /**
                             * Setter
                             * @param {any} value
                             */
                            function set(value) {
                                // Jen při změně
                                if (private != value) {
                                    private = value;
                                    if (old.set) { old.set.apply(ref, [value]); }
                                    if (hasSet) { type.set.apply(bridge, [value, bridge]); }
                                }
                            }
                        
                            // Definice nového descriptoru
                            refOP.define(key, { get: get, set: set, enumerable: true, configurable: true });
                            private = old.value;
                        }

                        /**
                         * Vynutí aplikaci mapy na cíl i nad novým objektem
                         * @param {any} map
                         * @param {any} target
                         */
                        function enforceMapping(map, target) {
                            var private = ref[key];
                            var oref = OP(ref);
                            var descriptor = oref.getDescriptor(key);
                            oref.define(key, {
                                get: function () { return descriptor && descriptor.get ? descriptor.get.apply(ref, []) : private; },
                                set: function (value) {
                                    if (private != value) { private = value; applyMap(value, map, target); }
                                    if (descriptor && descriptor.set) { descriptor.set.apply(ref, [value]); }
                                }
                            });
                        }

                        /**
                         * Zpracuje mapu vnořených objektů.
                         * @param {object} map
                         * @param {object} target
                         */
                        function innerMap(map, target) {
                            if (ref[key] == undefined || typeof (ref[key]) != "object") { ref[key] = {}; }
                            applyMap(ref[key], map, target);
                            enforceMapping(map, target);
                        }

                        // Vnořený objekt + možná zkratka.
                        if (hasInner) { innerMap(type.inner, to()); }
                        // Vnořený objekt bez mapovacích metod.
                        if (!(hasTo || hasGet || hasSet || hasInner)) { innerMap(type, target); }
                    }
                });
            }

            applyMap(ref, map, target);
            return op;
        }

        /**
         * @callback WatchCallback
         * @param {any} [value]
         * @param {string} [key]
         * @param {object} [ref] 
         */
        /**
         * Provede operaci při změně hodnoty vlastnosti objektu. 
         * @param {string} key
         * @param {WatchCallback} fn
         */
        function watch(key, fn) {

            function inner(fn, key) {       
                // Získání původní
                var old = op.getValueModel(key);
                if (!old.configurable) { delete ref[key]; }
                var private = old.value;

                /** Getter */                
                function get() { return old.get ? old.get.apply(ref, []) : private; }

                /**
                 * Setter
                 * @param {any} value
                 */
                function set(value) {
                    if (private != value) {
                        private = value;
                        if (old.set) { old.set.apply(ref, [value]); }
                        fn(value, key, ref);
                    }
                }

                // Definice nového descriptoru
                op.define(key, { get: get, set: set, enumerable: old.enumerable, configurable: true });
                ref[key] = old.value;
            }
            var args = arguments;
            args.length == 2 ? inner(args[1], args[0]) : OP(args[0]).each(inner);
            return op;
        }

        /**
         * @class ObjectProxy
         */
        var op = new (function ObjectProxy() {
            this.is = is;
            this.apply = apply;
            this.get = get;
            this.set = set;
            this.define = define;
            this.defineFinal = defineFinal;
            this.defineForce = defineForce;
            this.getDescriptor = getDescriptor;
            this.getValueModel = getValueModel;
            this.each = each;
            this.map = map;
            this.watch = watch;
        });
        return op;
    }
    var OP = ObjectProxy;
    return ObjectProxy;
})();



/**
 * MiddleWare pro komunikaci mezi kontexty knihoven.
 * @author Jan Pecha
 */
var MW = (function () {
    
    /**
     * @class TwoWayMap
     * Obousměrná mapa mezi vlastnostmi koncových objektů.
     * @param {object} options
     * @param {object} options.end1 - Odkaz na koncový objekt 1.
     * @param {object} options.map1 - Mapa z koncového objektu 1.
     * @param {object} options.data1 - Data koncového objektu 1.
     * @param {object} options.end2 - Odkaz na koncový objekt 2.
     * @param {object} options.map2 - Mapa z koncového objektu 2.
     * @param {object} options.data2 - Data koncového objektu 2.
     */
    function TwoWayMap(options) {
        OP(options.end1).map(options.map1, options.end2).set(options.data1);
        OP(options.end2).map(options.map2, options.end1).set(options.data2);
    }

    function async(fn) { setTimeout(fn, 0); }

    /**
     * @class SyncWorker
     * @param {function} fn - Vniřtní funkce SyncWorker
     */
    function SyncWorker(fn, isAsync) {

        function pasync(fn) { isAsync ? async(fn) : fn(); }

        var outer = this;
        // Listenery
        var listeners = { inner: { message: [], error: [] }, outer: { message: [], error: [] } };

        // Vnitřní API
        var inner = OP(new (function SyncWorkerContext() { }))
            .defineFinal({
                postMessage: function (message) {
                    pasync(function () {
                        try {
                            if (typeof outer.onmessage === "function") { pasync(function () { outer.onmessage(message); }); }
                            OP(listeners.outer.message).each(function (listener) { pasync(function () { listener(message); }); });
                        } catch (e) {
                            if (typeof inner.onerror === "function") { pasync(function () { inner.onerror(e); }); }
                            OP(listeners.inner.error).each(function (listener) { pasync(function () { listener(e); }); });
                            console.warn(e);
                        }
                    });
                },
                addEventListener: function (type, listener) { listeners.inner[type].push(listener); },
                removeEventListener: function (type, listener) { listeners.inner[type].splice(listeners.inner[type].indexOf(listener), 1); }
            })
            .set({ onerror: function () { }, onmessage: function () { } })
            .get();
        
        // Vnější API
        OP(outer)
            .defineFinal({
                postMessage: function (message) {
                    async(function () {
                        try {
                            if (typeof inner.onmessage === "function") { pasync(function () { inner.onmessage(message); }); }
                            OP(listeners.inner.message).each(function (listener) { async(function () { listener(message); }); });
                        } catch (e) {
                            if (typeof outer.onerror === "function") { pasync(function () { outer.onerror(e); }); }
                            OP(listeners.outer.error).each(function (listener) { pasync(function () { listener(e); }); });
                        }
                    });
                },
                addEventListener: function (type, listener) { listeners.outer[type].push(listener); },
                removeEventListener: function (type, listener) { listeners.outer[type].splice(listeners.outer[type].indexOf(listener), 1); }
            })
            .set({ onerror: function () { }, onmessage: function () { } });

        // Init
        async(function () {
            try {
                fn.apply(inner, [inner])
            } catch (e) {
                if (typeof outer.onerror === "function") { pasync(function () { outer.onerror(e); }); }
                OP(listeners.outer.error).each(function (listener) { pasync(function () { listener(e); }); });
                console.warn(e);
            }
        });
    }

    /**
     * @class MiddleWare
     */
    return new (function MiddleWare() {
        this.TwoWayMap = TwoWayMap;
        this.SyncWorker = SyncWorker;
    });
})();