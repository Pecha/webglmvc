﻿function setupScene(element) {
    var canvas = typeof (element) === "string" ? document.getElementById(element) : element;
    var scene = new THREE.Scene();
    scene.background = new THREE.Color(0.13, 0.13, 0.13);

    var renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    renderer.setSize(canvas.offsetWidth, canvas.offsetHeight);
    canvas.appendChild(renderer.domElement);
    scene.canvas = renderer.domElement;

    var camera = new THREE.PerspectiveCamera(45, canvas.offsetWidth / canvas.offsetHeight, 0.1, 1000);
    var controls = new THREE.OrbitControls(camera, canvas);
    camera.position.z = 16;
    scene.camera = camera;

    var light = new THREE.PointLight(0xffffff);
    light.position.set(20, 20, 50);
    scene.add(light)

    function render() {
        requestAnimationFrame(render);
        renderer.render(scene, camera);
        controls.update();
    }
    render()

    return scene;
}

function setupClickable(scene) {
    var clickable = [];
    var raycaster = new THREE.Raycaster();
    var mouse = new THREE.Vector2();
    scene.canvas.addEventListener("click", function (e) {
        var rect = scene.canvas.getBoundingClientRect();
        mouse.x = (event.clientX - rect.left) / rect.width * 2 - 1;
        mouse.y = -((event.clientY - rect.top) / rect.height) * 2 + 1;
        raycaster.setFromCamera(mouse, scene.camera);
        var intersects = raycaster.intersectObjects(clickable, true);
        if (intersects.length > 0) {
            intersects[0].object.onclick();
        }
    });
    return clickable;
}



var MAP = {

    TEXT: {
        to3DModel: function (font) {
            return {
                text: function (v, m) {
                    m().geometry = new THREE.TextGeometry(v, {
                        font: font,
                        size: 2,
                        height: 0.5,
                        curveSegments: 10
                    });
                    m().geometry.center();
                },
                diffuseColor: (v, m) => m().material.color.setStyle(v),
                emissiveColor: (v, m) => m().material.emissive.setStyle(v)
            }
        },
        toDataModel: {
            geometry: (v, m) => m().text = v.parameters.text,
            material: {
                color: (v, m) => m().diffuseColor = "#" + v.getHexString(),
                emissive: (v, m) => m().emissiveColor = "#" + v.getHexString()
            }
        }
    },

    CUBE: {
        to3DModel: {
            size: {
                get: m => m().scale,
                set: (v, m) => m().scale = v
            },
            position: {
                to: m => m.position,
                inner: {
                    x: {
                        get: m => (m().x * 1) - (m.root.ref.index * 2),
                        set: (v, m) => m().x = (m.root.ref.index * 2) + (v * 1),
                    },
                    y: (v, m) => m().y = v,
                    z: (v, m) => m().z = v
                }
            },
            color: (v, m) => m().material.color.setStyle(v)
        },
        toDataModel: {
            scale: (v, m) => m().size = v,
            position: {
                to: m => m.position,
                inner: {
                    x: (v, m) => m().x = (v * 1) - (m.root.target.index * 2),
                    y: (v, m) => m().y = v,
                    z: (v, m) => m().z = v
                }
            },
            material: {
                color: (v, m) => m().color = "#" + v.getHexString()
            }
        }
    }
};