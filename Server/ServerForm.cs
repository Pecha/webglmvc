﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    public partial class ServerForm : Form
    {
        private WebServer server = new WebServer();
        private Timer timer = new Timer();
        private string toLog = "";

        public ServerForm()
        {
            InitializeComponent();
            buttonRun.Text = "▶ Start";
            buttonRun.BackColor = Color.Green;
            textBoxUrls.Text = "http://localhost:8080/";
            server.Log = (s) => Log(s);
            timer.Interval = 100;
            timer.Tick += (Object myObject, EventArgs myEventArgs) =>
            {
                if (toLog != string.Empty)
                {
                    icon.ForeColor = Color.Orange;
                    textBoxLog.Text += toLog;
                    toLog = string.Empty;
                    textBoxLog.SelectionStart = textBoxLog.Text.Length;
                    textBoxLog.ScrollToCaret();
                    if (server.IsRunning)
                    {
                        buttonRun.Text = "◼ Stop";
                        buttonRun.BackColor = Color.Red;
                    }
                    else
                    {
                        buttonRun.Text = "▶ Start";
                        buttonRun.BackColor = Color.Green;
                    }
                } 
                else
                {
                    if (server.IsRunning)
                    {
                        icon.ForeColor = Color.Green;
                    }
                    else
                    {
                        icon.ForeColor = SystemColors.ControlDark;
                    }
                }
            };
            timer.Start();
        }

        public string Log(string message)
        {
            toLog += $"\r\n{message}";
            return message;
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            if(server.IsRunning)
            {
                server.Stop();
            } 
            else
            {
                server.Start(textBoxUrls.Lines);
            }
        }

        private void ServerForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            server.Stop();
            timer.Stop();
        }
    }
}
