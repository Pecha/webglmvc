﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    class WebServer
    {
        private bool running = false;
        private HttpListener listener;

        public Func<HttpListenerRequest, HttpListenerResponse, string> Handler;
        public Func<string, string> Log = (s) => s;

        public WebServer()
        {
            if (!HttpListener.IsSupported)
            {
                Log("Server is not supported");
            }
            Handler = (HttpListenerRequest request, HttpListenerResponse response) =>
            {
                string path = request.Url.LocalPath;
                Log(path);
                if (Regex.IsMatch(path, @".*?\/$"))
                {
                    path += "index.html";
                }
                path = Directory.GetCurrentDirectory() + path;
                if (File.Exists(path))
                {
                    response.ContentType = WebServer.GetContentType(Path.GetExtension(path));
                    using (FileStream fs = File.OpenRead(path))
                    {
                        response.ContentLength64 = fs.Length;
                        response.SendChunked = false;

                        byte[] buffer = new byte[64 * 1024];
                        int read;
                        using (BinaryWriter bw = new BinaryWriter(response.OutputStream))
                        {
                            while ((read = fs.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                bw.Write(buffer, 0, read);
                            }
                            bw.Close();
                        }

                        response.StatusCode = (int)HttpStatusCode.OK;
                        response.StatusDescription = "OK";
                    }
                }
                else
                {
                    Log("File not found");
                }
                return "OK";
            };
        }

        public void Start(params string[] prefixes)
        {
            listener = new HttpListener();
            try
            {
                foreach(string prefix in prefixes)
                {
                    listener.Prefixes.Add(prefix);
                }
                listener.Start();
                running = true;
                Log("Server started");
                Process.Start(prefixes[0]);
                ThreadPool.QueueUserWorkItem((o) =>
                {
                    try
                    {
                        while (listener.IsListening && IsRunning)
                        {
                            ThreadPool.QueueUserWorkItem((x) =>
                            {
                                var ctx = x as HttpListenerContext;
                                try
                                {
                                    Handler(ctx.Request, ctx.Response);
                                }
                                catch (Exception e)
                                {
                                    Log(e.Message);
                                }
                                finally
                                {
                                    ctx.Response.OutputStream.Close();
                                }
                            }, listener.GetContext());
                        }
                    }
                    catch (Exception e)
                    {
                        Log(e.Message);
                    }
                });
            }
            catch (Exception e)
            {
                Log(e.Message);
                Stop();
            }
        }

        public void Stop()
        {
            running = false;
            Log("Server stopped");
            if (listener != null)
            {
                listener.Abort();
                listener.Close();
            }
        }

        public bool IsRunning => running;

        public static string GetContentType(string extension)
        {
            switch (extension)
            {
                case ".html": return "text/html";
                case ".txt": return "text/plain";
                case ".css": return "text/css";
                case ".xml": return "text/xml";
                case ".csv": return "text/csv";
                case ".pdf": return "application/pdf";
                case ".zip": return "application/zip";
                case ".js": return "application/javascript";
                case ".json": return "application/json";
                case ".png": return "image/png";
                case ".svg": return "image/svg+xml";
                case ".jpg":
                case ".jpeg": return "image/jpeg";
                case ".gif": return "image/gif";
                case ".dds": return "image/dds";
                case ".mp4": return "video/mp4";
                case ".mp3": return "audio/mpeg";
                case ".wav": return "audio/vnd.wave";
                case ".jar": return "application/java-archive";
                case ".apk": return "application/vnd.android.package-archive";
                case ".cmd":
                case ".exe": return "application/x-msdownload";
                default: return "text/plain";
            }
        }
    }
}
